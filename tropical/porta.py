# Python module for generating files that can be processed by PORTA.

import numpy as np
import os

from typing import List, Tuple
from tropical.polynomials import tpoly

inf = float('inf')

PORTA_KEYWORDS = ['DIM',
                  'END',
                  'COMMENT',
                  'VALID',
                  'INEQUALITIES_SECTION',
                  'LOWER_BOUNDS',
                  'UPPER_BOUNDS',
                  'ELIMINATION_ORDER',
                  'CONV_SECTION',
                  'CONE_SECTION',
                  ]

def to_poi_file(list_of_points: List[Tuple[int, ...]], filename: str) -> int:
    """Create a .poi file describing the convex hull of a given list of points.

    This procedure takes a list of integer points and creates a .poi file
    (interpretable by PORTA) describing the convex hull of the points of the
    list.

    Call:
        >>> dim = to_poi_file(list_of_points, filename)

    Input:
        - list_of_points (list): a list of integer points represented as tuples
        of the same length.
        - filename (str): the name to give to the created file.
    
    Output:
        - dim (int): the dimension of the polytope obtained by taking the
        convex hull of all the points.
    """
    n = len(list_of_points[0])
    for point in list_of_points[1:]:
        if len(point) != n:
            raise ValueError('All points must have the same number of '
                             'coordinates.')
    if filename[-4:] != '.poi': filename += '.poi'
    file = open(filename, 'x')
    file.write(f'DIM = {n}\n\n')
    file.write('CONV_SECTION\n')
    for point in list_of_points:
        for coordinate in point[:n-1]:
            file.write(f'{coordinate} ')
        file.write(f'{point[n-1]}\n')
    file.write(f'\nEND')
    m = len(list_of_points)
    mat = np.zeros((n, m-1))
    origin = np.array(list_of_points[0])
    for index, point in enumerate(list_of_points[1:]):
        mat[:,index] = np.array(point) - origin
    dim = np.linalg.matrix_rank(mat)
    file.write(f'\nDIMENSION OF THE POLYHEDRON : {dim}')
    file.close()
    return dim

def get_striped_lines(file) -> List[str]:
    """For getting the lines of a .poi or .ieq file with all extra whitespace
    and all comments after 'END' removed.
    """
    file_lines = []
    for line in map(str.strip, file.readlines()):
        if line: file_lines += [line]
        if line == 'END': break
    # Optional syntactic tests    
    # if not file_lines[0].startswith('DIM'):
    #     raise ValueError(f'Missing DIM at the beginning of {file.name}.')
    # if file_lines[-1] != 'END':
    #     raise ValueError(f'Missing END at the end of {file.name}.')
    return file_lines

def find_index_keyword(file_lines: List[str], keyword: str) -> int:
    """
    """
    if keyword not in PORTA_KEYWORDS:
        raise ValueError(f'Argument {keyword} is not a PORTA keyword.')
    for index, line in enumerate(file_lines):
        if line.startswith(keyword): return index
    raise (f'Keyword {keyword} not found.')

def lines_until_next_keyword(file_lines: List[str], start_index: int) -> int:
    """
    """
    for index, line in enumerate(file_lines[start_index+1:]):
        if line[0].isalpha():
            # Optional syntactic test
            # if line not in PORTA_KEYWORDS:
            #     raise ValueError('Unknown keyword found.')
            return index

def get_inequality_from_line(line: str,
                             n: int,
                             debug: bool = False,
                             ) -> Tuple[List[str], str, str]:
    """
    """
    lhs = ['' for i in range(n)]
    line = ''.join(line.split())
    start_index = 0
    if line[0] == '(':
        while line[start_index] != ')':
            start_index += 1
        start_index += 1
    accumulator = ''
    for index, char in enumerate(line[start_index:]):
        if debug: print(f'{index = }\n{char = }\n{accumulator = }\n\n')
        if char == 'x':
            # The commented section would be used to get the actual number
            # values of the coefficients of the equation and not just their
            # string representation.
            # try:
            #     if len(accumulator.split('/')) == 2:
            #         numerator, denominator = accumulator.split('/')
            #         coeff = int(numerator)/int(denominator)
            #     else:
            #         coeff = int(accumulator)
            #     accumulator = ''
            # except ValueError:
            #     if accumulator == '-':
            #         coeff = -1
            #     elif accumulator == '+' or accumulator == '':
            #         coeff = +1
            #     accumulator = ''
            coeff = accumulator
            accumulator = ''
        elif char in ['+', '-']:
            try:
                varno = int(accumulator)
                lhs[varno-1] = coeff
            except ValueError:
                if accumulator == '':
                    pass
            accumulator = char
        elif char in ['<', '=', '>']:
            try:
                varno = int(accumulator)
                lhs[varno-1] = coeff
            except ValueError:
                if accumulator == '':
                    pass
            end_index = index + start_index
            break
        else:
            accumulator += char
    if line[end_index+1] in ['<', '=', '>']:
        rel = line[end_index:end_index+2]
        if debug: print(f'{rel = }\n')
        end_index += 2
    else:
        rel = line[end_index]
        if debug: print(f'{rel = }\n')
        end_index += 1
    rhs = line[end_index:]
    return lhs, rel, rhs

def get_inequalities_from_ieq_file(ieq_file,
                                   debug: bool = False,
                                   ) -> Tuple[List[List[str]],
                                              List[str],
                                              List[str]]:
    """For getting the inequalities from a .ieq file stored in a numpy array
    """
    file_lines = get_striped_lines(ieq_file)
    if debug: print(f'{file_lines = }')
    n = int(file_lines[0].split()[2])  # This assumes that the first line of
    # ieq_file is precisely of the form 'DIM␣=␣<some integer>↲', and will
    # otherwise issue an error.
    if debug: print(f'{n = }')
    start_index = find_index_keyword(file_lines, 'INEQUALITIES_SECTION')
    if debug: print(f'{start_index = }')
    k = lines_until_next_keyword(file_lines, start_index)  # This is the number of inequalities
    # involved in the input file.
    if debug: print(f'{k = }')
    end_index = start_index + k + 1
    lhs = [['' for i in range(n)] for j in range(k)]
    rel = ['' for j in range(k)]
    rhs = ['' for j in range(k)]
    for index, line in enumerate(file_lines[start_index+1:end_index]):
        lhs[index], rel[index], rhs[index] = get_inequality_from_line(line,
                                                                         n)
    return lhs, rel, rhs


def minkowski_sum_as_ieq_file(list_of_supports: List[List[Tuple[int, ...]]],
                              filename: str,
                              debug: bool = False
                              ) -> None:
    """
    """
    n = len(list_of_supports[0][0])
    for support in list_of_supports[1:]:
        if len(support[0]) != n:
            raise ValueError('All points must have the same number of '
                             'coordinates.')
    k = len(list_of_supports)
    lower_bounds = ()
    upper_bounds = ()
    for i in range(n):
        lower_bounds += (sum(
            min(point[i] for point in support) for support in list_of_supports
            ),)
        upper_bounds += (sum(
            max(point[i] for point in support) for support in list_of_supports
            ),)
    elimination_order = ()
    for i in range(n):
        elimination_order += (0,)
    for i in range(k*n):
        elimination_order += (k*n-i,)
    dim = 0
    for i in range(k):
        dim += to_poi_file(list_of_supports[i], f'support{i}.poi')
        os.system(f'xporta -T support{i}.poi')
    if filename[-4:] != '.ieq': filename += '.ieq'
    file = open(filename, 'x')
    file.write(f'DIM = {(k+1)*n}\n\n')
    # To be used for the actual Minkowski sum, i.e. after projection
    # file.write('LOWER_BOUNDS\n')
    # for lb in lower_bounds[:n-1]:
    #     file.write(f'{lb} ')
    # file.write(f'{lower_bounds[n-1]}\n')
    # file.write('UPPER_BOUNDS\n')
    # for ub in upper_bounds[:n-1]:
    #     file.write(f'{ub} ')
    # file.write(f'{upper_bounds[n-1]}\n\n')
    file.write('ELIMINATION_ORDER\n')
    for num in elimination_order[:(k+1)*n-1]:
        file.write(f'{num} ')
    file.write(f'{elimination_order[(k+1)*n-1]}\n\n')
    file.write('INEQUALITIES_SECTION\n')
    lhs = []
    rel = []
    rhs = []
    for i in range(k):
        file_i = open(f'support{i}.poi.ieq', 'r')
        lhs_i, rel_i, rhs_i = get_inequalities_from_ieq_file(file_i)
        if debug:
            print(f'{lhs_i =}')
            print(f'{rel_i =}')
            print(f'{rhs_i =}')
        for r in range(len(lhs_i)):
            lhs += [['' for j in range(n)]
                    + ['' for j in range(n*i)]
                    + lhs_i[r]
                    + ['' for j in range(n*(i+1), k*n)]]
            rel += [rel_i[r]]
            rhs += [rhs_i[r]]
        file_i.close()
    for i in range(k):
        os.remove(f'support{i}.poi')
        os.remove(f'support{i}.poi.ieq')
    for i in range(n):
        lhs += [
            ['-' if j==i else '+' if j%n==i else '' for j in range((k+1)*n)]
            ]
        rel += ['==']
        rhs += ['0']
    if debug:
        print(f'{lhs =}')
        print(f'{rel =}')
        print(f'{rhs =}')
    for eqno, expression in enumerate(lhs):
        for varno, coeff in enumerate(expression):
            if coeff: file.write(coeff+f'x{varno+1}')
        file.write(f' {rel[eqno]} {rhs[eqno]}\n')
    file.write('\nEND\n')
    file.write(f'DIMENSION OF THE POLYHEDRON : {dim}')  # Note that this
    # assumes in particular that the Minkowski sum of the polytope is full
    # dimensional, which is generically true.
    file.close()
    os.system(f'xporta -F {filename}')
    os.remove(f'{filename}')
    temp_file = open(f'{filename}.ieq', 'r')
    res_file_lines = get_striped_lines(temp_file)
    lower_bounds_str = f'{lower_bounds[0]}'
    for e in lower_bounds[1:]:
        lower_bounds_str += f' {e}'
    upper_bounds_str = f'{upper_bounds[0]}'
    for e in upper_bounds[1:]:
        upper_bounds_str += f' {e}'
    res_file_lines = [f'DIM = {n}',
                      'LOWER_BOUNDS',
                      lower_bounds_str,
                      'UPPER_BOUNDS',
                      upper_bounds_str,
                      ] + res_file_lines[1:]
    start_index = find_index_keyword(res_file_lines, 'INEQUALITIES_SECTION')
    if debug: print(f'{start_index = }')
    l = lines_until_next_keyword(res_file_lines, start_index)  # This is the
    # number of inequalities involved in the result file.
    if debug: print(f'{l = }')
    end_index = start_index + l + 1
    for index, line in enumerate(res_file_lines[start_index+1:end_index]):
        lhs, _, _ = get_inequality_from_line(line, n)
        is_empty = True
        for elt in lhs:
            if elt:
                is_empty = False
                break
        if is_empty:
            res_file_lines[start_index + index + 1] = ''
    res_file_lines += [f'DIMENSION OF THE POLYHEDRON : {n}']
    res_file = open(f'{filename}', 'x')
    for line in res_file_lines:
        if line:
            if line[0] == '(':
                start = 1
                while line[start] != ')':
                    start += 1
                start += 1
                res_file.write(line[start:]+'\n')
            else:
                res_file.write(line+'\n')
    temp_file.close()
    os.remove(f'{filename}.ieq')
    res_file.close()

def get_list_points_from_poi(poi_file) -> List[Tuple[int, ...]]:
    """
    """
    file_lines = get_striped_lines(poi_file)
    start_index = find_index_keyword(file_lines, 'CONV_SECTION') + 1
    nb_points = lines_until_next_keyword(file_lines, start_index)
    end_index = start_index + nb_points + 1
    list_points = []
    for line in file_lines[start_index:end_index]:
        point = ()
        start = 0
        if line[0] == '(':
            start = 1
            while line[start] != ')':
                start += 1
            start += 1
        for char in line[start:].split():
            point += (int(char),)
        list_points += [point]
    return list_points




#==============================================================================
#----------------------------------- Tests ------------------------------------
#==============================================================================

def test():
    f = tpoly({(0, 0): 1, (1, 0): 2, (0, 1): 1, (1, 1): 1},'x y')
    print(f'{f = }\n')
    A = f.support
    print(f'{A = }\n')
    to_poi_file(A, 'test.poi')
    poi_file = open('test.poi', 'r')
    print('Contents of the created file:')
    print(poi_file.read())
    poi_file.close()
    os.remove('test.poi')

    list_of_supports = [[(0, 0, 0), (0, 0, 1), (0, 1, 0), (0, 1, 1),
                         (1, 0, 0), (1, 0, 1), (1, 1, 0), (1, 1, 1)],
                        [(0, 0, 0), (0, 0, 1)],
                        ]

    
    minkowski_sum_as_ieq_file(list_of_supports, 'minkowski.ieq')
    ieq_file = open('minkowski.ieq', 'r')
    ieq_file_contents = ieq_file.read()
    ieq_file.close()
    print(ieq_file_contents)
    os.system('valid -V minkowski.ieq')
    poi_file = open('minkowski.poi', 'r')
    list_points = get_list_points_from_poi(poi_file)
    poi_file_contents = poi_file.read()
    print(poi_file_contents)
    print(f'{list_points = }')
    os.remove('minkowski.ieq')
    os.remove('minkowski.poi')
    