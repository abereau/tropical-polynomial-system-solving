# Python module implementing tropical matrices and tropical linear algebra.

from __future__ import annotations

import numpy as np
from math import floor, log10
from typing import Dict, List, Optional, Tuple, Union

inf = float('inf')

# TODO: Créer un alias tropical matrix englobant tarray et sptmat

#==============================================================================
#------------------------------- Full matrices --------------------------------
#==============================================================================

# TODO: Changer le nom de la classe en TropArray ou TropMat
class tarray(np.ndarray):
    """Implement tropical matrices. 
    
    This class is a subclass of the ndarray class from the module numpy which
    overloads the usual operations on numpy arrays to replace them with their
    tropical equivalent.
    """

    def __new__(cls, input_array: np.ndarray):
        obj = np.asarray(input_array).view(cls)
        return obj

    def __array_finalize__(self, obj):
        return None

#    def __init__(self, input_array: np.ndarray):
#        """Construct a new tropical array from a regular one.
# 
#        Call:
#            >>> tmat = tarray(input_array)

#        Input:
#            - input_array (ndarray): the initializing array.

#        Output:
#            - tmat (tarray): the same array but viewed as a tropical object.
#        """
#        self.values = np.asarray(input_array)

#    def __str__(self) -> str:
#        """Display a tropical array."""
#        return np.ndarray.__str__(self)

#    def __repr__(self) -> str:
#        """Display a tropical array."""
#        return np.ndarray.__repr__(self)

    def __add__(self, other: tarray) -> tarray:
        """Return the sum of two tropical arrays.

        The sum of two tropical arrays is defined as the maximum operator
        applied coefficient-wise.
        """
        return np.maximum(self, other).view(tarray)
    
    # TODO : implement __iadd__

#    def __neg__(self):
#        """Return the (classical) opposite of a tropical array."""
#        return -self

    def __mul__(self, other: tarray) -> tarray:
        """Return the coefficient-wise product of two tropical arrays.

        The coefficient-wise product of two tropical arrays is defined as their
        (classical) sum.
        """
        return np.add(self, other).view(tarray)

    def __pow__(self, n: int) -> tarray:
        """Return the coefficient-wise n-th power of a tropical array.
        
        The coefficient-wise n-th power of a tropical array is defined as its
        classical sum with itself n times.
        """
        return np.mul(n, self).view(tarray)

    def __matmul__(self, other: tarray) -> tarray:
        """Return the tropical matrix multiplication of two tropical arrays.

        Tropical matrix multiplication is implemented according to the
        definition of tropical operations on the scalars. For two 1D arrays,
        their tropical matrix multiplication correspond to their tropical dot
        product. For a 2D array and a 1D array, it correspond to the tropical
        multiplication of a matrix and a vector. For two 2D arrays, it
        corresponds to the tropical multiplication of two matrices.
        
        Note that we do not implement tropical array multiplication for higher
        dimensional arrays (3D or more).
        """
        if len(self.shape) >= 3 or len(other.shape) >= 3: return NotImplemented
        if len(self.shape) == 1:
            if self.shape[0] != other.shape[0]:
                raise ValueError('Matrices are not aligned.')
            if len(other.shape) == 1:
                dot = float('-inf')
                for i in range(self.shape[0]):
                    dot = max(dot, self[i] + other[i])
                return tarray([dot])
            if len(other.shape) == 2:
                nbCols = other.shape[1]
                res = zeros(nbCols)
                for j in range(nbCols):
                    for i in range(self.shape[0]):
                        res[j] = max(res[j], self[i] + other[i, j])
                return res
        if len(other.shape) == 1:
            if self.shape[1] != other.shape[0]:
                raise ValueError('Matrices are not aligned.')
            nbRows = self.shape[1]
            res = zeros(nbRows)
            for i in range(self.shape[0]):
                for j in range(nbRows):
                    res[i] = max(res[i], self[i, j] + other[j])
            return res
        if self.shape[1] != other.shape[0]:
            raise ValueError('Matrices are not aligned.')
        nbRows, nbCols = self.shape[0], other.shape[1]
        res=zeros((nbRows, nbCols))
        for i in range(nbRows):
            for j in range(nbCols):
                for k in range(self.shape[1]):
                    res[i, j] = max(res[i, j], self[i, k] + other[k, j])
        return res
    
    def dot(self, other: tarray) -> tarray:
        """Return the product of a tropical matrix with a tropical vector.

        Call:
            >>> y = A.dot(x)

        Input:
            - A (2D tarray): a tropical matrix.
            - x (1D tarray): a tropical vector.

        Output:
            - y (1D tarray): the result of the tropical matrix multiplication.
        """
        nr, nc = self.shape
        if other.shape[0] != nc: raise ValueError('Matrices are not aligned.')
        res = zeros(nr)
        for i in range(nr):
            for j in range(nc):
                res[i] = max(res[i], self[i, j] + other[j])
                # TODO : Maybe handle the case where self[i,j] == inf
        return res
    # TODO: unifier avec __matmul__

    def sharp(self, other: tarray) -> tarray:
        """Return the product of a dual tropical matrix with a tropical vector.

        Call:
            >>> y = A.sharp(x)
            
        Input:
            - A (2D tarray): a tropical matrix.
            - x (1D tarray): a tropical vector.

        Output:
            - y (1D tarray): the result of the tropical matrix multiplication.
        """
        nr, nc = self.shape
        if other.shape[0] != nr: raise ValueError('Matrices are not aligned.')
        res = full(nc, inf)
        for i in range(nr):
            for j in range(nc):
                res[j] = min(res[j], -self[i, j] + other[i])
                # TODO : Maybe handle the case where self[i, j] == inf
        return res

def empty(shape: Tuple[int, ...]) -> tarray:
    """Return an uninitialized tropical array of given shape."""
    return np.empty(shape).view(tarray)

def full(shape: Tuple[int, ...], fill_value: float) -> tarray:
    """Construct a tropical array of given shape filled with a given value."""
    return np.full(shape, fill_value).view(tarray)

def zeros(shape: Tuple[int, ...]) -> tarray:
    """Construct a tropical array of given shape filled with -inf."""
    return np.full(shape, float('-inf')).view(tarray)

def ones(shape: Tuple[int, ...]) -> tarray:
    """Construct a tropical array of given shape filled with 0."""
    return np.zeros(shape).view(tarray)

def eye(nr: int, nc: Optional[int] = None, shift: int = 0) -> tarray:
    """Construct a (2-D) tropical identity matrix with given shape.
    
    Call:
        >>> mat = eye(nr, nc, shift)

    Input:
        - nr (int): the number of rows of the constructed matrix.
        - nc (int or None, default None): the number of columns of the
        constructed matrix. When equal to None, the constructed matrix is
        square.
        - shift (int, default 0): shifts the diagonal of the matrix by the
        given value. A positive value shifts the diagonal upwards and a
        negative value shifts it downwards.
    """
    nbRows = nr
    nbCols = nr if nc is None else nc
    mat = zeros((nbRows, nbCols))
    if shift >= 0:
        for i in range(min(nbRows, nbCols - shift)):
            mat[i, i + shift] = 0
    else:
        for i in range(min(nbRows + shift, nbCols)):
            mat[i - shift, i] = 0
    return mat

def max_twice(list_: List[float]) -> bool:
    """Test whether the maximum in a given list is attained at least twice."""
    max1, max2 = -inf, -inf
    for elt in list_:
        if elt >= max1: max1, max2 = elt, max1
    return max1 == max2
# TODO: à déplacer dans un module plus général.

def null(M: Union[tarray,sptmat], y: tarray) -> bool:
    """Test whether a vector is in the tropical kernel of a tropical matrix.

    Call:
        >>> res = null(M, y)

    Input:
        - M (2D tarray or sptmat): a tropical matrix.
        - y (1D tarray): a tropical vector.
        
    Output:
        - res (bool): whether y is in the kernel of the matrix M or not.
    """
    nr, nc = M.shape
    n = len(y)
    if n != nc: raise ValueError('Matrices are not aligned.')
    res = max_twice([[M[0, j] + y[j] for j in range(nc)]])
    i = 1
    while res is True and i <= nr-1:
        res = max_twice([[M[i, j] + y[j] for j in range(nc)]])
        i += 1
    return res

#==============================================================================
#------------------------------ Sparse matrices -------------------------------
#==============================================================================

# TODO: Changer le nom de la classe en SparTropMat
class sptmat:
    """Implement sparse tropical matrices.
    
    This class implements sparse tropical matrices with a dictionary of keys
    representation.
    """

    def __init__(self, input_dict: Dict[Tuple[int, int], float],
                 nr: int, nc: int):
        """Construct a new sparse tropical matrix.

        Call:
            >>> mat = sptmat(input_dict, nr, nc)

        Input:
            - input_dict (dict): the keys represent the indices of the
            coefficients of the constructed matrix and the entries correspond
            to the values of the associated coefficients.
            - nr (int): the number of rows of the constructed matrix.
            - nc (int): the number of columns of the constructed matrix.
        """
        val_dict = {}
        for key in input_dict:
            if input_dict[key] != -inf: val_dict[key] = input_dict[key]
            # The unnecessary zero coefficients are not kept in memory.
        indices = val_dict.keys()
        for index in indices:
            if index[0] >= nr or index[1] >= nc:
                raise IndexError('Index out of range.')
        self.__values = val_dict
        self.__nb_rows = nr
        self.__nb_cols = nc
    
    @property
    def values(self) -> Dict[Tuple[int, int], float]:
        """Get the values of the coefficients of a sparse tropical matrix.
        
        The result is in the form of a dictionary whose keys are the indices of
        the coefficients of the matrix, and whose entries represent the values
        of these coefficients.

        Call:
            >>> val_dict = mat.values

        Input:
            - mat (sptmat): a sparse tropical matrix.

        Output:
            - val_dict (dict): the dictionary of values of the matrix.
        """
        return self.__values

    @property
    def shape(self) -> Tuple[int, int]:
        """Get the dimensions of a sparse tropical matrix.

        Call:
            >>> nr,nc = mat.shape

        Input:
            - mat (sptmat): a sparse tropical matrix.

        Output:
            - nr, nc (int, int): respectively the number of rows and of columns
            of the matrix.
        """
        return self.__nb_rows, self.__nb_cols
    
    @property
    def support(self) -> List[Tuple[int, int]]: # The support is unsorted.
        """Get the support of a sparse tropical matrix.
        
        The support of a sparse tropical matrix consists in the list of indices
        of nonzero coefficients of the matrix.

        Call:
            >>> supp = mat.support

        Input:
            - mat (sptmat): a sparse tropical matrix.

        Output:
            - supp (list): the support of the matrix.
        """
        return self.values.keys()
    
    def __str__(self) -> str:
        """Display a sparse tropical matrix.
        
        The matrix is displayed as by listing the indices and values of the
        nonzero coefficients in lexicographical order of the indices.
        """
        nr, nc = self.shape
        display = ''
        for index in sorted(self.support):
            row_index = f'{index[0]}'
            col_index = f'{index[1]}'
            coefficient = f'{self.values[index]}'
            display += (
                '(' + row_index.rjust(floor(log10(nr-1))+1) + ', '
                + col_index.rjust(floor(log10(nc-1))+1) + ') '
                + coefficient.rjust(8) + '\n'
                )
        display += f'\nNumber of rows    : {nr}\nNumber of columns : {nc}\n'
        return display
    
    def __repr__(self) -> str:
        """Display a sparse tropical matrix.
        
        The matrix is displayed as by listing the indices and values of the
        nonzero coefficients in lexicographical order of the indices.
        """
        nr, nc = self.shape
        display = ''
        for index in sorted(self.support):
            row_index = f'{index[0]}'
            col_index = f'{index[1]}'
            coefficient = f'{self.values[index]}'
            display += (
                '(' + row_index.rjust(floor(log10(nr-1))+1) + ', '
                + col_index.rjust(floor(log10(nc-1))+1) + ') '
                + coefficient.rjust(8) + '\n'
                )
        display += f'Number of rows    : {nr}\nNumber of columns : {nc}\n'
        return display
    
    def __getitem__(self, key: Tuple[int, int]) -> float:
        """Get a given coefficient of a sparse tropical matrix.

        Call:
            >>> coeff = mat[key]

        Input:
            - key (int, int): the indices of the selected coefficient of the
            matrix.

        Output:
            - coeff (float): the value of the selected coefficient.
        """
        nr, nc = self.shape
        if key[0] >= nr or key[1] >= nc:
            raise IndexError('Index out of range.')
        return self.values.get(key, -inf)
    # TODO : improve to handle slicing
    
    def __setitem__(self, key: Tuple[int, int], value: float):
        """Set the value of a given coefficient in a sparse tropical matrix.

        Call:
            >>> mat[key] = value

        Input:
            - key (int, int): the indices of the selected coefficient of the
            matrix.
            - value (float): the prescribed value of the coefficient.
        """
        nr, nc = self.shape
        if key[0] >= nr or key[1] >= nc:
            raise IndexError('Index out of range.')
        if value == -inf:
            if key in self.values: del self.values[key]
        else:
            self.values[key] = value

    def __add__(self, other: sptmat) -> sptmat:
        """Return the sum of two sparse tropical matrices.
        
        The addition of two sparse tropical matrices is defined as the maximum
        operator applied coefficient-wise.
        """
        nr, nc = self.shape
        if other.shape != (nr, nc):
            raise ValueError('The two matrices must have the same shape.')
        sum_dict = {}
        for key in self.support:
            sum_dict[key] = self[key]
        for key in other.support:
            if other[key] > sum_dict.get(key, -inf): sum_dict[key] = other[key]
        return sptmat(sum_dict, nr, nc)
    
    # TODO : implement __iadd__

    # def __neg__(self) -> sptmat:
    #     """Return the (classical) opposite of a sparse tropical matrix."""
    #     nr, nc = self.shape
    #     neg_dict = {}
    #     for key in self.support:
    #         neg_dict[key] = -self[key]
    #     return sptmat(neg_dict, nr, nc)

    def __rmul__(self, other: float) -> sptmat:
        """Return the product of a sparse tropical matrix by a scalar.
        
        The tropical product of a matrix by a scalar is defined as the matrix
        obtained by adding the scalar to every coefficient of the given matrix.
        """
        nr, nc = self.shape
        prod_dict = {}
        if other != -inf:
            for key in self.support:
                prod_dict[key] = self[key] + other
        return sptmat(prod_dict, nr, nc)

    def transpose(self) -> sptmat:
        """Return the transpose of a sparse tropical matrix."""
        nr, nc = self.shape
        trans_dict = {}
        for i, j in self.support:
            trans_dict[j, i] = self[i, j]
        return sptmat(trans_dict, nc, nr)

    def dot(self, other: tarray) -> tarray:
        """Return the product of a sparse tropical matrix with a vector.

        Call:
            >>> y = A.dot(x)

        Input:
            - A (sptmat): a sparse tropical matrix.
            - x (1D tarray): a tropical vector.

        Output:
            - y (1D tarray): the result of the tropical matrix multiplication.
        """
        nr, nc = self.shape
        if other.shape[0] != nc: raise ValueError('Matrices are not aligned.')
        res = zeros(nr)
        for i, j in self.support:
            res[i] = max(res[i], self[i, j] + other[j])
            # TODO : Maybe handle the case where self[i, j] == inf
        return res

    def sharp(self, other: tarray) -> tarray:
        """Return the product of a dual sparse tropical matrix with a vector.

        Call:
            >>> y = A.sharp(x)
            
        Input:
            - A (sptmat): a sparse tropical matrix.
            - x (1D tarray): a tropical vector.

        Output:
            - y (1D tarray): the result of the tropical matrix multiplication.
        """
        nr, nc = self.shape
        if other.shape[0] != nr: raise ValueError('Matrices are not aligned.')
        res = full(nc, inf)
        for i, j in self.support:
            res[j] = min(res[j], -self[i,j] + other[i])
            # TODO : Maybe handle the case where self[i, j] == inf
        return res

#==============================================================================
#--------------------------- Matrix type conversion ---------------------------
#==============================================================================

def to_sparse(fmat: tarray) -> sptmat:
    """Construct a sparse tropical matrix from a full tropical matrix.

    Call:
        >>> smat = to_sparse(fmat))

    Input:
        - fmat (2D tarray): a full tropical matrix.

    Output:
        - smat (sptmat): the sparse representation of the input matrix.
    """
    val_dict = {}
    nr, nc = fmat.shape
    for i in range(nr):
        for j in range(nc):
            if fmat[i, j] != -inf: val_dict[i, j] = fmat[i, j]
    return sptmat(val_dict, nr, nc)

def to_full(smat: sptmat) -> tarray:
    """Construct a full tropical matrix from a sparse tropical matrix.

    Call:
        >>> fmat = to_full(smat)

    Input:
        -smat (sptmat): a sparse tropical matrix.

    Output:
        - fmat (2D tarray): the full representation of the input matrix.
    """
    nr, nc = smat.shape
    mat_full = zeros((nr, nc))
    for key in smat.support:
        mat_full[key] = smat[key]
    return mat_full

#==============================================================================
#----------------------------------- Tests ------------------------------------
#==============================================================================

def test():
    A = sptmat({(2, 1): 4, (3, 4): -1, (3, 1): 1, (4, 4): 3}, 5, 5)
    print(f'A =\n{A}')
    print(f'A.values = {A.values}')
    print(f'A.support = {A.support}')
    A[0, 0] = 2
    print(f'\n\nA =\n{A}')
    print(f'A.values = {A.values}')
    print(f'A.support = {A.support}')
    print(f'2*A =\n{2*A}')
    print(f'-inf*A =\n{-inf*A}')
    B = sptmat({(2, 1): -2, (0, 4): 0, (3, 1): -1, (3, 4): -4}, 5, 5)
    print(f'B =\n{B}')
    C = A+B
    print(f'C =\n{C}')
    D = C.transpose()
    print(f'D  =\n{D}')
    D[0, 0] = -inf
    print(f'D  =\n{D}')
    E = to_full(D)
    print(f'E =\n{E}')
    print(f'type(E) = {type(E)}')
    print(f'E + E = {E+E}')
    F = E.view(np.ndarray)
    print(f'type(F) = {type(F)}')
    print(f'F + F = {F+F}')

    T = zeros((4, 4))
    T[2, 2] = 0
    print(f'T =\n{T}')
    U = to_sparse(T)
    print(f'U =\n{U}')

    v = zeros(2)
    print(f'v =\n{v}')
    print(f'v.shape = {v.shape}')