# Python module implementing the construction of tropical Macaulay matrices.

from itertools import product
from time import time
from copy import deepcopy
from typing import Dict, List, Optional, Tuple

import tropical.matrices as tmat
import tropical.polynomials as tpol

#==============================================================================
#-------------------- Operations on exponents of monomials --------------------
#==============================================================================

def tuple_add(alpha: Tuple[float, ...],
              beta: Tuple[float, ...]) -> Tuple[float, ...]:
    """Return the coordinate-wise sum of two tuples."""
    return tuple(a + b for a, b in zip(alpha ,beta))

def tuple_sub(alpha: Tuple[float, ...],
              beta: Tuple[float, ...]) -> Tuple[float, ...]:
    """Return the coordinate-wise difference of two tuples."""
    return tuple(a - b for a,b in zip(alpha, beta))

def tuple_sum(list_tuple: List[Tuple[float, ...]]) -> Tuple[float, ...]:
    """Return the coordinate-wise sum of a list of tuple."""
    return tuple(sum(x) for x in zip(*list_tuple))

def tuple_neg(alpha: Tuple[float, ...]) -> Tuple[float, ...]:
    """Return the coordinate-wise opposite of a tuple."""
    return tuple(-a for a in alpha)

#==============================================================================
#------- Auxillary functions for the construction of Macaulay matrices --------
#==============================================================================

def iter_mons(n: int,
              N: int,
              i: Optional[int] = None
              ) -> List[Tuple[int, ...]]:
    """Return the list of all the monomials of degree below a given bound.
    
    The elements of the returned list consist of tuples corresponding to the
    exponents of the monomials, ordered in the graded lexicographic order.
    
    Call:
        >>> list_mons = iter_mons(n, N)
    Input:
        - n (int): the number of variables of the monomials.
        - N (int): the bound on the degree of the monomials.
        - i (int or None, default None): a prefix to prepend to all the tuples
        of the list whenever i is not None.
    Output:
        - list_mons (list): the exponents of all monomials in n variables and
        of total degree bounded by N, possibly prefixed by the number i.
    """
    if i is None:
        return [
            beta for beta in sorted(list(product(range(N, -1, -1), repeat=n)),
                                    key=sum)
            if sum(beta) <= N
            ]
    return [(i, beta) for beta in iter_mons(n, N)]

def enum_mons(n: int,
              N: int
              ) -> Dict[Tuple[int, ...], int]:
    """Index all the monomials of degree below a given bound.
    
    This function returns a dictionary whose keys correspond to the exponents
    in a fixed number of variables and with a bounded degree, and whose entries
    correspond to their position (starting at 0) in the graded lexicographic
    order.
    
    Call:
        >>> enum = enum_mons(n, N)

    Input:
        - n (int): the number of variables of the monomials.
        - N (int): the bound on the degree of the monomials.

    Output:
        - enum (dict): the dictionary of exponents of monomials indexed by
        their position in the graded lexicographic order.
    """
    enum = {}
    mon = iter_mons(n, N)
    for i in range(len(mon)):
        enum[mon[i]] = i
    return enum

def enum_mons_list(list_mons: List[Tuple[int, ...]]
                   ) -> Dict[Tuple[int, ...], int]:
    """Index all monomials according to their position in the input list
    
    This function eturns a dictionary whose keys correspond to the exponents of
    the input list and whose entries correspond their positions in the input
    list.
    
    Call:
        >>> enum = enum_mons(list_mons)

    Input:
        - list_mons (list): the list of exponents of monomials.

    Output:
        - enum (dict): the dictionary of exponents of monomials indexed by
        their position in the input list.
    """
    enum = {}
    for i in range(len(list_mons)):
        enum[list_mons[i]] = i
    return enum

#==============================================================================
#---------------------------- Full Macaulay matrix ----------------------------
#==============================================================================

def macaulay_full(f: List[tpol.tpoly],
                  N: int,
                  debug: bool = False
                  ) -> tmat.tarray:
    """Construct the full Macaulay matrix of a list of tropical polynomials.
    
    This function returns the full Macaulay matrix, truncated to a given degree
    bound, associated to a collection of tropical polynomials.

    Call:
        >>> mac = macaulay_full(f, N)

    Input:
        - f (list): a collection of tropical polynomials.
        - N (int): the truncation degree of the Macaulay matrix.
        - debug (bool, default False): for activating the debug mode, which
        displays the contents of different intermediate variables.

    Output:
        - mac (2D tarray): the full Macaulay matrix associated to the
        collection f.
    """
    k = len(f)
    if debug: print(f'k = {k}')
    n = f[0].nb_variables
    for i in range(1, k):
        if f[i].nb_variables != n:
            raise ValueError('All polynomials must have the same number of '
                             'variables.')
    if debug: print(f'n = {n}')
    deg = [f[i].degree() for i in range(k)]
    if debug: print(f'deg = {deg}')
    cols = iter_mons(n, N)
    if debug: print(f'cols = {cols}')
    nb_cols = len(cols)
    if debug: print(f'nb_cols = {nb_cols}')
    rows = []
    for i in range(k):
        rows += iter_mons(n, N-deg[i], i)
    if debug: print(f'rows = {rows}')
    nb_rows = len(rows)
    if debug: print(f'nb_rows = {nb_rows}')
    mac = tmat.zeros((nb_rows, nb_cols))
    for p in range(nb_rows):
        i, alpha = rows[p]
        for q in range(nb_cols):
            beta = cols[q]
            mac[p, q] = f[i][tuple_sub(beta, alpha)]
    return mac

#==============================================================================
#--------------------------- Sparse Macaulay matrix ---------------------------
#==============================================================================

def macaulay_sparse(f: List[tpol.tpoly],
                    cols: List[Tuple[int, ...]],
                    timer: bool = False,
                    debug: bool = False,
                    ) -> tmat.sptmat:
    """Construct the sparse Macaulay matrix of a list of tropical polynomials.
    
    This function returns the sparse submatrix, obtained by restricting to a
    finite subset of columns of the Macaulay matrix associated to a collection
    of tropical polynomials, and to the rows whose support is included in said
    columns.

    Call:
        >>> M = macaulay_sparse(f, cols)

    Input:
        - f (list): a list of tropical polynomials.
        - cols (list): a liste of tuples corresponding to the columns of the
        constructed matrix.
        - timer (bool, default False): for activating a timer.
        - debug (bool, default False): for activating the debug mode, which
        displays the contents of different intermediate variables.

    Output:
        - M (sptmat): the sparse Macaulay matrix associated to the collection 
        f.
    """
    k = len(f)
    if debug: print(f'k = {k}')
    n = f[0].nb_variables
    for i in range(1, k):
        if f[i].nb_variables != n:
            raise ValueError('All polynomials must have the same number of '
                             'variables.')
    # Maybe raise an error if the elements of cols are not n-tuples.
    if debug: print(f'n = {n}')
    list_supports = [f[i].support for i in range(k)]
    if debug: print(f'list_supports = {list_supports}')
    enum_cols = enum_mons_list(cols)
    if debug: print(f'enum_cols = {enum_cols}')
    t1 = time()
    rows = []
    for i in range(k):
        for beta in cols:
            alpha = tuple_sub(beta, list_supports[i][0])
            if all(comp >= 0 for comp in alpha):
                row_in_matrix = True
                for delta in list_supports[i][:0:-1]:
                    if not tuple_add(delta, alpha) in enum_cols:                    
                        row_in_matrix = False
                        break
                if row_in_matrix:
                    rows += [(i, *alpha)]
    enum_rows = enum_mons_list(rows)
    if debug: print(f'enum_rows = {enum_rows}')
    t2 = time()
    if timer: print(f'Calcul des lignes de la matrice de Macaulay : '
                    f'fait en {t2-t1}s.')
    t3 = time()
    nr, nc = len(rows), len(cols)
    M = tmat.sptmat({}, nr, nc)
    for row in rows:
        i, alpha = row[0], row[1:]
        for delta in list_supports[i]:
            beta = tuple_add(delta, alpha)
            M[enum_rows[row], enum_cols[beta]] = f[i][tuple_sub(beta, alpha)]
    t4 = time()
    if timer: print(f'Calcul de la matrice de Macaulay : fait en {t4-t3}s.')
    file = open('macaulay_matrix.txt', 'w')
    file.write(f'{M}\n')
    return M

def macaulay_sparse_2(fp: List[tpol.tpoly],
                      fm: List[tpol.tpoly],
                      cols: List[Tuple[int, ...]],
                      timer: bool = False,
                      debug: bool = False,
                      ) -> Tuple[tmat.sptmat, tmat.sptmat]:
    """Construct the sparse Macaulay matrices of a pair of lists of tropical
    polynomials.
    
    This function returns the pair of sparse submatrices of the Macaulay
    matrices associated to a collection of tropical polynomial inequalities of
    the form fp[i](x) >= fm[i](x), obtained by restricting the set of columns.

    Call:
        >>> Mp, Mm = macaulay_sparse(fp, fm, cols)

    Input:
        - fp (list): a list of tropical polynomials representing the
         lefthandside of the inequality fp >= fm.
        - fm (list): a list of tropical polynomials representing the
        righthandside of the inequality fp >= fm.
        - cols (list): a liste of tuples corresponding to the columns of the
        constructed matrix.
        - timer (bool, default False): for activating a timer.
        - debug (bool, default False): for activating the debug mode, which
        displays the contents of different intermediate variables.

    Output:
        - Mp, Mm (sptmat, sptmat): the pair of sparse Macaulay matrices
        associated to the inequality fp >= fm.
    """
    k = len(fp)
    if len(fm) != k:
        raise ValueError('The two lists must contain the same number of '
                         'polynomials.')
    if debug: print(f'k = {k}')
    f = [fp[i] + fm[i] for i in range(k)]
    n = f[0].nb_variables
    for i in range(1, k):
        if f[i].nb_variables != n:
            raise ValueError('All polynomials must have the same number of '
                             'variables.')
    # Maybe raise an error if the elements of cols are not n-tuples.
    if debug: print(f'n = {n}')
    list_supports = [f[i].support for i in range(k)]
    if debug: print(f'list_supports = {list_supports}')
    enum_cols = enum_mons_list(cols)
    if debug: print(f'enum_cols = {enum_cols}')
    t1 = time()
    rows = []
    for i in range(k):
        for beta in cols:
            alpha = tuple_sub(beta, list_supports[i][0])
            if all(comp >= 0 for comp in alpha):
                row_in_matrix = True
                for delta in list_supports[i][:0:-1]:
                    if not tuple_add(delta, alpha) in enum_cols:                    
                        row_in_matrix = False
                        break
                if row_in_matrix:
                    rows += [(i, *alpha)]
    enum_rows = enum_mons_list(rows)
    if debug: print(f'enum_rows = {enum_rows}')
    t2 = time()
    if timer: print(f'Rows of the Macaulay matrix '
                    f'computed in: {t2-t1:.4e}s.')
    t3 = time()
    nr, nc = len(rows), len(cols)
    Mp = tmat.sptmat({}, nr, nc)
    Mm = tmat.sptmat({}, nr, nc)
    for row in rows:
        i, alpha = row[0],row[1:]
        for delta in list_supports[i]:
            beta = tuple_add(delta, alpha)
            Mp[enum_rows[row], enum_cols[beta]] = fp[i][tuple_sub(beta, alpha)]
            Mm[enum_rows[row], enum_cols[beta]] = fm[i][tuple_sub(beta, alpha)]
    t4 = time()
    if timer: print(f'Coefficients of the Macaulay matrix '
                    f'computed in: {t4-t3:.4e}s.')
    file = open('macaulay_matrix.txt', 'w')
    file.write(f'{Mp}\n{Mm}\n')
    return Mp, Mm

def macaulay_sparse_2_dichotomy(fp: List[tpol.tpoly],
                                fm: List[tpol.tpoly],
                                cols: List[Tuple[int, ...]],
                                timer: bool = False,
                                debug: bool = False,
                                ) -> Tuple[List[tmat.sptmat],
                                           List[tmat.sptmat]]:
    """Construct the sparse Macaulay matrix of a list of tropical polynomials
    in a format suited for dichotomy search of the solutions.
    
    This function returns the pair of sparse submatrices of the Macaulay
    matrices associated to a collection of tropical polynomial inequalities of
    the form fp[i](x) >= fm[i](x) with additional constraints of the form
    b >= x[i] and x[i] >= a. The rows of the Macaulay matrices are separated in
    groups and stored in two lists. The rows corresponding to the inequality
    fp[i](x) >= fm[i](x) for all i are stocked in the first coefficient of the
    list, then the rows corresponding to constraints of the form b >= x[i] and
    x[i] >= a are respectively stored in the (2i-1)-th and (2i)-th coefficient
    of the list. This way of storing the matrices allow for incremental
    dichotomy search of the solutions of the inequality fp >= fm.

    Call:
        >>> Mp_split, Mm_split = macaulay_sparse(fp, fm, cols)

    Input:
        - fp (list): a list of tropical polynomials representing the
         lefthandside of the inequality fp >= fm.
        - fm (list): a list of tropical polynomials representing the
        righthandside of the inequality fp >= fm.
        - cols (list): a liste of tuples corresponding to the columns of the
        constructed matrix.
        - timer (bool, default False): for activating a timer.
        - debug (bool, default False): for activating the debug mode, which
        displays the contents of different intermediate variables.

    Output:
        - Mp_split, Mm_split (list, list): the pair of sparse Macaulay matrices
        associated to the inequality fp >= fm sliced and stored in a pair of
        lists
    """
    k = len(fp)
    if len(fm) != k:
        raise ValueError('The two lists must contain the same number of '
                         'polynomials.')
    if debug: print(f'k = {k}')
    f = [fp[i] + fm[i] for i in range(k)]
    n = f[0].nb_variables
    if debug: print(f'n = {n}')
    list_var = f[0].variables
    for i in range(1, k):
        if f[i].variables != list_var:
            raise ValueError('All polynomials must have the same variables.')
    # Maybe raise an error if the elements of cols are not n-tuples.
    var = ' '.join(list_var)
    g = []
    for i in range(n):
        e_i = [0 for i in range(n)]
        e_i[i] = 1
        g += [tpol.tpoly({tuple(e_i): 0}, var)]
    if debug: print(f'g = {g}')
    cst = tpol.tpoly({tuple(0 for i in range(n)): 0}, var)
    if debug: print(f'cst = {cst}')
    Fp = deepcopy(fp)
    Fm = deepcopy(fm)
    F = deepcopy(f)
    for i in range(n):
        Fp += [cst, g[i]]
        Fm += [g[i], cst]
        F += [g[i]+cst, g[i]+cst]
    list_supports = [F[i].support for i in range(k+2*n)]
    if debug: print(f'list_supports = {list_supports}')
    enum_cols = enum_mons_list(cols)
    if debug: print(f'enum_cols = {enum_cols}')
    t1 = time()
    rows = []
    slice = []
    for j in range(k):
        for beta in cols:
            alpha = tuple_sub(beta, list_supports[j][0])
            if all(comp >= 0 for comp in alpha):
                row_in_matrix = True
                for delta in list_supports[j][:0:-1]:
                    if not tuple_add(delta, alpha) in enum_cols:                    
                        row_in_matrix = False
                        break
                if row_in_matrix:
                    rows += [(j, *alpha)]
    slice += [len(rows)]
    for i in range(n):
        for beta in cols:
            alpha = tuple_sub(beta, list_supports[k+2*i][0])
            if all(comp >= 0 for comp in alpha):
                row_in_matrix = True
                for delta in list_supports[k+2*i][:0:-1]:
                    if not tuple_add(delta, alpha) in enum_cols:                    
                        row_in_matrix = False
                        break
                if row_in_matrix:
                    rows += [(k+2*i, *alpha)]
        slice += [len(rows)]
        for beta in cols:
            alpha = tuple_sub(beta, list_supports[k+2*i+1][0])
            if all(comp >= 0 for comp in alpha):
                row_in_matrix = True
                for delta in list_supports[k+2*i+1][:0:-1]:
                    if not tuple_add(delta, alpha) in enum_cols:                    
                        row_in_matrix = False
                        break
                if row_in_matrix:
                    rows += [(k+2*i+1, *alpha)]
        slice += [len(rows)]
    if debug: print(f'slice = {slice}')
    enum_rows = enum_mons_list(rows)
    if debug: print(f'enum_rows = {enum_rows}')
    t2 = time()
    if timer: print(f'Calcul des lignes de la matrice de Macaulay : '
                    f'fait en {t2-t1}s.')
    t3 = time()
    nr, nc = len(rows), len(cols)
    Mp_split = [tmat.sptmat({}, nr, nc) for s in range(2*n+1)]
    Mm_split = [tmat.sptmat({}, nr, nc) for s in range(2*n+1)]
    s = 0
    for row in rows:
        i, alpha = row[0], row[1:]
        row_index = enum_rows[row]
        if row_index >= slice[s]:
            s += 1
        for delta in list_supports[i]:
            beta = tuple_add(delta, alpha)
            col_index = enum_cols[beta]
            Mp_split[s][row_index, col_index] = Fp[i][tuple_sub(beta, alpha)]
            Mm_split[s][row_index, col_index] = Fm[i][tuple_sub(beta, alpha)]
    t4 = time()
    if timer: print(f'Calcul de la matrice de Macaulay : fait en {t4-t3}s.')
    # file = open('macaulay_matrix.txt', 'w')
    # file.write(f'{Mp}\n{Mm}\n')
    return Mp_split, Mm_split

# TODO:
# - Write a separate function for the two-sided case in order to avoid
# repeating calculations.
# - Write a function to_scicoslab to export a sparse tropical matrix in the
# scicoslab format
# - Handle the balance case.