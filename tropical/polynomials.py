# Python module implementing tropical polynomials.

from __future__ import annotations

from random import randint, uniform, sample
from scipy.special import binom
from itertools import product
from typing import Dict, List, Literal, Optional, Tuple

import polytope.polytope as alg

inf = float('inf')

#==============================================================================
#-------------------- Tools for the display of polynomials --------------------
#==============================================================================

def to_sup(n: int) -> str:
    """Convert an integer into a string of superscript digits."""
    sups = {'0': '\u2070', '1': '\xb9', '2': '\xb2', '3': '\xb3',
            '4': '\u2074', '5': '\u2075', '6': '\u2076', '7': '\u2077',
            '8': '\u2078', '9': '\u2079'}
    return ''.join(sups.get(char, char) for char in str(n))

def to_sub(n: int) -> str:
    """Convert an integer into a string of subscript digits."""
    subs = {'0': '\u2080', '1': '\u2081', '2': '\u2082', '3': '\u2083',
            '4': '\u2084', '5': '\u2085', '6': '\u2086', '7': '\u2087',
            '8': '\u2088', '9': '\u2089'}
    return ''.join(subs.get(char, char) for char in str(n))

#==============================================================================
#---------------------------- Tropical polynomials ----------------------------
#==============================================================================

# TODO: Changer le nom de la classe en TropPoly
class tpoly:
    """Implement multivariate tropical polynomials in sparse notation."""

    def __init__(self, input_dict: Dict[Tuple[int, ...], float], var: str):
        """Construct a new sparse tropical mutlivariate polynomial.

        Call:
            >>> p = tpoly(input_dict, var)

        Input:
            - input_dict (dict): the keys represent the exponents of the
            monomials of p and whose entries correspond to the values of the
            associated coefficients.
            - var (str): the variable names separated by spaces.

        Output:
            - p (tpoly): the constructed tropical polynomial.
        """
        list_var = var.split()
        nb_var = len(list_var)
        if len(set(list_var)) < nb_var:
            raise ValueError('The variables must have distinct names.')
        val_dict = {}
        for key in input_dict:
            if input_dict[key] != -inf: val_dict[key] = input_dict[key]
            # The unnecessary zero coefficients are not kept in memory.
        exponents = val_dict.keys()
        for e in exponents:
            if len(e) != nb_var:
                raise ValueError('All keys of the input dictionary must have '
                                 'the same length.')
        self.__variables = list_var
        self.__values = val_dict
        self.__support = sorted(exponents, key=lambda x: (sum(x), x[::-1]))
        # TODO : probably unnecessary to keep the sorted support as an
        # attribute + maybe make the support a set instead of a list.
    
    @property
    def variables(self) -> List[str]:
        """Get the variables of a tropical polynomial in a list.

        Call:
            >>> list_var = p.variables

        Output:
            - list_var (list): the list of variables of p, where each element
            of the list is a string representing a variable.
        """
        return self.__variables
    
    @property
    def nb_variables(self) -> int:
        """Get the number of variables of a tropical polynomial.

        Call:
            >>> nb_var = p.nb_variables

        Output:
            - nb_var (int): the number of variables of p.
        """
        return len(self.variables)
        
    @property
    def values(self) -> Dict[Tuple[int, ...], float]:
        """Get the coefficients of a tropical polynomial in a dictionary.

        Call:
            >>> val_dict = p.values

        Output:
            - val_dict (dict): a dictionary whose keys are integer tuples
            representing the exponents of monomials and whose entries represent
            the associated coefficients.
        """
        return self.__values
    
    @property
    def support(self) -> List[Tuple[int, ...]]:
        """Get the support of a tropical polynomial in a list.

        Call:
            >>> exponents = p.support

        Output:
            - exponents (list): a list of integer tuples corresponding to the
            exponents of the monomials of the polynomial.
        """
        return self.__support
        # TODO: It should be a set rather than a list

    def __str__(self) -> str:
        """Display a tropical polynomial.
        
        The tropical polynomial is represented as a string in which the
        tropical addition is represented by the symbol '⨁' and the tropical
        multiplication is simplyb represented by concatenation.
        """
        exponents = self.support
        var = self.variables
        nb_var = self.nb_variables
        if exponents == []: return '-\u221e'
        alpha = exponents[0]
        display = str(self[alpha])
        for i in range(nb_var):
            if alpha[i] == 1: display += var[i]
            if alpha[i] >= 2: display += var[i]+to_sup(alpha[i])
        for alpha in exponents[1:]:
            display += ' \u2a01 '+str(self[alpha])
            for i in range(nb_var):
                if alpha[i] == 1: display += var[i]
                if alpha[i] >= 2: display += var[i]+to_sup(alpha[i])
        return display

    def __repr__(self) -> str:
        """Display a tropical polynomial.
        
        The tropical polynomial is represented as a string in which the
        tropical addition is represented by the symbol '⨁' and the tropical
        multiplication is simplyb represented by concatenation.
        """
        exponents = self.support
        var = self.variables
        nb_var = self.nb_variables
        if exponents == []: return '-\u221e'
        alpha = exponents[0]
        display = str(self[alpha])
        for i in range(nb_var):
            if alpha[i] == 1: display += var[i]
            if alpha[i] >= 2: display += var[i]+to_sup(alpha[i])
        for alpha in exponents[1:]:
            display += ' \u2a01 '+str(self[alpha])
            for i in range(nb_var):
                if alpha[i] == 1: display += var[i]
                if alpha[i] >= 2: display += var[i]+to_sup(alpha[i])
        return display

    def __getitem__(self, key: Tuple[int, ...]) -> float:
        """Get a given coefficient of a tropical polynomial.

        Call:
            >>> coeff = p[alpha]

        Input:
            - alpha (tuple): the exposant of a monomial of p.

        Output:
            - coeff (float): the coefficient of exponant alpha in the
            polynomial p (in particular this is -inf whenever  alpha is not in
            the support of p).
        """
        if len(key) != self.nb_variables:
            raise KeyError('Incorrect number of indices in the key.')
        return self.values.get(key, -inf)

    def __add__(self, other: tpoly) -> tpoly:
        """Return the sum of two tropical polynomials.

        The addition of two tropical polynomials is defined as the maximum
        operator applied coefficient-wise.
        """
        list_var = self.variables
        if other.variables != list_var:
            raise ValueError(
                'The two polynomials must have the same variables.'
                )
        sum_var = ' '.join(list_var)
        sum_supp = list(set(self.support).union(set(other.support)))
        sum_dict = {}
        for alpha in sum_supp:
            sum_dict[alpha] = max(self[alpha], other[alpha])
        return tpoly(sum_dict, sum_var)

    # TODO : implement __iadd__

    def degree(self) -> float:  # TODO: make it a property
        """Return the total degree of a tropical polynomial.
        
        The total degree is the maximum of the degrees of each monomial, that
        is the maximum of the sum of the exponents of each monomial.
        """
        if self.support: return max(sum(alpha) for alpha in self.support)
        return -inf

    def __mul__(self, other: tpoly) -> tpoly:
        """Return the product of two tropical polynomials.
        
        The product of two tropical polynomials is defined as their Cauchy
        product, except the sum is replaced by a maximum and the products in
        the sum by additions.
        """
        list_var = self.variables
        if other.variables != list_var:
            raise ValueError(
                'The two polynomials must have the same variables.'
                )
        prod_var = ' '.join(list_var)
        prod_dict = {}
        for alpha in self.support:
            for beta in other.support:
                gamma = tuple(a + b for a, b in zip(alpha, beta))
                prod_dict[gamma] = max(prod_dict.get(gamma, -inf),
                                       self[alpha] + other[beta])
        return tpoly(prod_dict, prod_var)
    
    def __call__(self, x: Tuple[float, ...]) -> float:
        """Return the evaluation of a tropical polynomial at a given point x.

        Call:
            >>> val = p(x)

        Input:
            - x (float): the point at which the polynomial p is evaluated.

        Output:
            - val (float): the evaluation of p at point x.
        """
        n = self.nb_variables
        if len(x) != n:
            raise ValueError('The number of coordinates of the entry does not '
                             'match the number of variables.')
        return max(self[alpha] + sum(x[i]*alpha[i] for i in range(n))
                   for alpha in self.support)
    
    def is_root(self, x: Tuple[float, ...]) -> bool:
        """Decide whether a point x is a root of a tropical polynomial.
        
        Call:
            >>> res = p.is_root(x)

        Input:
            - x (float): the point at which the polynomial p is evaluated.

        Output:
            - res (bool): whether x is a root of the tropical polynomial p.
        """
        n = self.nb_variables
        if len(x) != n:
            raise ValueError('The number of coordinates of the entry does not '
                             'match the number of variables.')
        max1, max2 = -inf, -inf
        list_terms=[self[alpha] + sum(x[i]*alpha[i] for i in range(n))
                    for alpha in self.support]
        for term in list_terms:
            if term >= max1: max1, max2 = term, max1
        return max1 == max2
        # TODO: réécrire à l'aide de la fonction max_twice
    
    def newton_polytope(self) -> alg.Polytope:
        """Return the Newton polytope of a sparse tropical polynomial.

        The Newton polytope of a (sparse) tropical polynomial is the convex
        hull of the set of exponents of the monomials of the polynomial. This
        convex hull is computed using the Python module polytope.
        
        Call:
            >>> Q = p.newton_polytope()

        Output:
            - Q (Polytope): the Newton polytope of p.
        """
        return alg.qhull(self.support)

#==============================================================================
#------------------------ Random polynomial generation ------------------------
#==============================================================================

def rand_tpoly(var: str,
               deg: int,
               spar: int,
               mode: Literal[0, 1] = 0,
               bound: float = 100,
               debug: bool = False,
               ) -> tpoly:
    """Generate a random tropical polynomial of given degree and sparsity.
    
    Call:
        >>> p = rand_tpoly(var, deg, spar, mode, bound)

    Input:
        - var (str): the variables of the generated polynomial.
        - deg (int): the degree of the generated polynomial.
        - spar (int): the sparsity of the generated polynomial.
        - mode (int, default 0): 0 or 1, mode=0 is for generating tropical
        polynomials with integer coefficients, mode=1 is for generating
        tropical polynomials with real coefficients.
        - bound (int, default 100): bound on the absolute value of the coefficients
        of the generated polynomial.
        - debug (bool, default False): whether or not to activate the debug
        mode, which prints out the contents of all the variables involved in
        the function.

    Output:
        - p (tpoly): the randomly generated polynomaial.
    """
    n = len(var.split())
    if spar > binom(deg+n-1, n-1): raise ValueError('Sparsity out of bound.')
    if mode == 0:
        rand = randint
    elif mode == 1:
        rand = uniform
    else: raise ValueError('The mode must be set to either 0 or 1.')
    #TODO: Exception redondante
    if debug: print(f'n = {n}')
    rand_dict = {}
    incorrect_degree = True
    while incorrect_degree:
        list_mons = sample(iter_mons(n, deg), spar)
        if debug: print(f'list_mons = {list_mons}')
        incorrect_degree = max(sum(x) for x in list_mons) < deg
    # TODO: Try to avoid multiple draws even though they are infrequent.
    for mon in list_mons:
        rand_dict[mon] = rand(-bound, bound)
    return tpoly(rand_dict, var)
    # TODO: Change the name of the modes

def rand_hom_tpoly(var: str,
                   deg: int,
                   spar: int,
                   mode: Literal[0, 1] = 0,
                   bound: float = 100,
                   debug: bool = False
                   ) -> tpoly:
    """Generate a random homogenous tropical polynomial of given degree and
    sparsity.
    
    Call:
        >>> p = rand_tpoly(var, deg, spar, mode, bound)

    Input:
        - var (str): the variables of the generated polynomial.
        - deg (int): the degree of the generated polynomial.
        - spar (int): the sparsity of the generated polynomial.
        - mode (int, default 0): 0 or 1, mode=0 is for generating tropical
        polynomials with integer coefficients, mode=1 is for generating
        tropical polynomials with real coefficients.
        - bound (int, default 100): bound on the absolute value of the coefficients
        of the generated polynomial.
        - debug (bool, default False): whether or not to activate the debug
        mode, which prints out the contents of all the variables involved in
        the function.

    Output:
        - p (tpoly): the randomly generated polynomaial.
    """
    n = len(var.split())
    if spar > binom(deg+n-1, n-1): raise ValueError('Sparsity out of bound.')
    if mode == 0:
        rand = randint
    elif mode == 1:
        rand = uniform
    else: raise ValueError('The mode must be set to either 0 or 1.')
    if debug: print(f'n = {n}')
    rand_dict = {}
    list_mons = list(map(lambda x: x + (deg-sum(x),),iter_mons(n-1, deg)))
    if debug: print(f'list_mons = {list_mons}')
    list_mons = sample(list_mons, spar)
    if debug: print(f'list_mons = {list_mons}')
    for mon in list_mons:
        rand_dict[mon] = rand(-bound, bound)
    return tpoly(rand_dict, var)

#==============================================================================
#--------------------------- Monomials enumeration ----------------------------
#==============================================================================

def iter_mons(n: int,
              N: int,
              i: Optional[int] = None
              ) -> List[Tuple[int, ...]]:
    """Return the list of all the monomials of degree below a given bound.
    
    The elements of the returned list consist of tuples corresponding to the
    exponents of the monomials, ordered in the graded lexicographic order.
    
    Call:
        >>> list_mons = iter_mons(n, N, i)
    Input:
        - n (int): the number of variables of the monomials.
        - N (int): the bound on the degree of the monomials.
        - i (int or None, default None): a prefix to prepend to all the tuples
        of the list whenever i is not None.
    Output:
        - list_mons (list): the exponents of all monomials in n variables and
        of total degree bounded by N, possibly prefixed by the number i.
    """
    if i is None:
        return [
            beta for beta in sorted(list(product(range(N, -1, -1), repeat=n)),
                                    key=sum)
            if sum(beta) <= N
            ]
    return [(i, beta) for beta in iter_mons(n, N)]

# TODO : ensembles de Canny-Emiris, option pour choisir l'ordre monomial,
# passer en min-plus, créer une classe pour gérer les exposants ou
# éventuellement passer par les numpy arrays

#==============================================================================
#----------------------------------- Tests ------------------------------------
#==============================================================================

# L'exemple suivant permet d'ordonner un dictionnaire. Permet éventuellement
# d'éviter la redondance entre les attributs .values et .support.
def test():
    test_dict = {(0, 0): 1, (1, 0): 2, (0, 1): 1, (1, 1): 1}
    f = tpoly(test_dict,'x y')
    print(f'f = {f}')
    print(f'test_dict = {test_dict}')
    test_dict[0, 2] = 5
    print(f'test_dict = {test_dict}')
    test_dict[2, 0] = -3
    print(f'test_dict = {test_dict}')
    print(f'test_dict.items() = {test_dict.items()}')
    sorted_dict = dict(
        sorted(test_dict.items(),
               key=lambda x: (sum(x[0]), x[0][::-1]))
        )
    print(f'sorted_dict = {sorted_dict}')
    g = tpoly(test_dict,'x y')
    print(f'g = {g}')
    print(f'f + g = {f+g}')
    print(f'g((0, 0)) = {g((0, 0))}')
    print(f'f((0, 0)) = {f((0, 0))}')