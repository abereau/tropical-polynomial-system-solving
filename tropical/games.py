# Python module implementing the main tools for tropical linear inequalities
# solving with mean payoff games tools.

import numpy as np
from time import time
from random import randint
from scipy.special import binom
from copy import deepcopy
from typing import Callable, List, Optional, Tuple, Union

from tropical.macaulay import *
import tropical.polynomials as tpol
import tropical.matrices as tmat

inf = float('inf')

def flatten(list_):
    """Flatten a list of lists.
    
    This function returns the list obtained by concatenating all the sublists
    of the input list in order.
    """
    return [item for sublist in list_ for item in sublist]

def two_max(list_: List[float],
            argmax: bool = False
            ) -> Union[Tuple[float, float], Tuple[float, float, bool, bool]]:
    """Return the maximum and second maximum of a list.
    
    If argmax is True, this function also returns the respective indices of the
    maxima. If the maximum is attained more than twice, then the indices
    of the last two occurences of the maximum are returned."""
    if len(list_) <= 1:
        raise ValueError('The input list must contain at least two elements.')
    if list_[0] >= list_[1]:    
        max1, max2 = list_[0], list_[1]
        argmax1, argmax2 = 0, 1
    else:
        max1, max2 = list_[1], list_[0]
        argmax1, argmax2 = 1, 0
    for index, elt in enumerate(list_[2:]):
        if elt >= max1:
            max1, max2 = elt, max1
            argmax1, argmax2 = index, argmax1
        elif elt >= max2:
            max2 = elt
            argmax2 = index
    if argmax: return max1, max2, argmax1, argmax2
    return max1, max2

def max_attained_twice(list_: List[float]) -> bool:
    """Test whether the maximum in a given list is attained at least twice."""
    max1, max2 = two_max(list_)
    return max1 == max2
# TODO: à déplacer dans un module plus général.

#==============================================================================
#------------------------------ Shapley operator ------------------------------
#==============================================================================

def shapley(M: Union[tmat.sptmat, tmat.tarray],
            ) -> Callable[[np.ndarray], np.ndarray]:
    """Construct the Shapley operator associated to the tropical matrix M.
    
    This function returns the Shapley operator associated to the tropical
    linear system given by the matrix M.

    Call:
        >>> T = shapley_full(M)

    Input:
        - M (sptmat or 2D tarray): a tropical matrix.

    Output:
        - T (function): the Shapley operator associated to the matrix M.
    """
    nr, nc = M.shape
    def T(y):
        """This is the Shapley operator associated to the matrix M."""
        Ty = np.array([inf for j in range(nc)])
        z1 = np.array([-inf for i in range(nr)])
        z2 = np.array([-inf for i in range(nr)])
        argmax = np.array([0 for i in range(nr)])
        for i in range(nr):
            max1, max2, argmax1, argmax2 = two_max(
                [M[i, k] + y[k] for k in range(nc)], argmax=True
                )
            z1[i] = max1
            z2[i] = max2
            argmax[i] = argmax1
        for j in range(nc):
            for i in range(nr):
                if argmax[i] != j:
                    Ty[j] = min(Ty[j], -M[i, j] + z1[i])
                else:
                    Ty[j] = min(Ty[j], -M[i, j] + z2[i])
        return Ty
    return T

def shapley_2(Mp: Union[tmat.sptmat, tmat.tarray],
              Mm: Union[tmat.sptmat, tmat.tarray],
              ) -> Callable[[np.ndarray], np.ndarray]:
    """Construct the Shapley operator associated to the inequality Mp >= Mm.
    
    This function returns the Shapley operator associated to the tropical
    linear inequality Mp >= Mm, where Mp et Mm are two tropical matrices.

    Call:
        >>> T = shapley_2(Mp, Mm)

    Input:
        - Mp (sptmat or 2D tarray): a tropical matrix.
        - Mm (sptmat or 2D tarray): a tropical matrix.

    Output:
        - T (function): the Shapley operator associated to the inequality
        Mp >= Mm.
    """
    if Mp.shape != Mm.shape:
        raise ValueError('The two matrices must have the same shape.')
    return lambda y: Mm.sharp(Mp.dot(y)).view(np.ndarray)

def shapley_full_2(fp: List[tpol.tpoly],
                   fm: List[tpol.tpoly],
                   N: int,
                   debug: bool = False,
                   ) -> Callable[[np.ndarray], np.ndarray]:
    """Construct the full Shapley operator associated to inequality fp >= fm.
    
    This function returns the full Shapley operator associated to the tropical
    linear inequality Mp >= Mm, where Mp et Mm are the respective full Macaulay
    matrices associated to the two collections (in the form of lists) fp and fm
    of multivariate tropical polynomials. The Macaulay matrices are truncated
    to the degree N.

    Call:
        >>> T = shapley_full_2(fp, fm, N)

    Input:
        - fp (list): a list of tropical polynomials representing the
         lefthandside of the inequality fp >= fm.
        - fm (list): a list of tropical polynomials representing the
        righthandside of the inequality fp >= fm.
        - N (int): the truncation degree of the Macaulay matrix.
        - debug (bool, default False): for activating the debug mode, which
        displays the contents of different intermediate variables.

    Output:
        - T (function): the full Shapley operator associated to the system
        fp >= fm.
    """
    k = len(fp)
    if len(fm) != k:
        raise ValueError('The two lists must contain the same number of '
                         'polynomials.')
    if debug: print(f'k = {k}')
    f = [fp[i] + fm[i] for i in range(k)]
    n = f[0].nb_variables
    deg = [f[i].degree() for i in range(k)]
    if debug: print(f'deg = {deg}')
    mons = iter_mons(n,N)
    if debug: print(f'mons = {mons}')
    enum_cols = enum_mons(n, N)
    if debug: print(f'enum_cols = {enum_cols}')
    iter_rows = flatten([iter_mons(n, N-deg[i], i) for i in range(k)])
    enum_rows = enum_mons_list(iter_rows)
    if debug: print(f'enum_rows = {enum_rows}')
    def T(y):
        """This is the Shapley operator associated to the system fp >= fm."""
        Ty = []
        mons = iter_mons(n, N)
        By = []
        for i in range(k):
            for alpha in iter_mons(n, N-deg[i]):
                by = -inf
                for beta in map(lambda x: tuple_add(alpha, x), fp[i].support):
                    if min(beta) >= 0 and sum(beta) <= N:
                        by = max(by, (fp[i][tuple_sub(beta, alpha)]
                                      + y[enum_cols[beta]]))
                By += [by]
        for gamma in mons:
            ty = inf
            for i in range(k):
                for alpha in map(lambda x: tuple_sub(gamma, x), fm[i].support):
                    if min(alpha) >= 0 and sum(alpha) <= N-deg[i]:
                        ty = min(ty, (-fm[i][tuple_sub(gamma,alpha)]
                                      + By[enum_rows[i,alpha]]))
            Ty += [ty]
        return Ty
    return T # TODO : Rewrite using full Macaulay matrices.

def shapley_sparse_2(fp: List[tpol.tpoly],
                     fm: List[tpol.tpoly],
                     cols: List[Tuple[int, ...]],
                     debug: bool = False,
                     ) -> Callable[[np.ndarray], np.ndarray]:
    """Construct the sparse Shapley operator associated to inequality fp >= fm.
    
    This function returns the sparse Shapley operator associated to the
    tropical linear inequality Mp >= Mm, where Mp et Mm are the respective
    sparse Macaulay matrices associated to the two collections (in the form of
    lists) fp and fm of multivariate tropical polynomials. The columns of the
    Macaulay matrices considered are given by the list cols.

    Call:
        >>> T = shapley_sparse_2(fp, fm, cols)

    Input:
        - fp (list): a list of tropical polynomials representing the
         lefthandside of the inequality fp >= fm.
        - fm (list): a list of tropical polynomials representing the
        righthandside of the inequality fp >= fm.
        - cols (list): a liste of tuples corresponding to the columns of the
        constructed matrix.
        - debug (bool, default False): for activating the debug mode, which
        displays the contents of different intermediate variables.

    Output:
        - T (function): the sparse Shapley operator associated to the system
        fp >= fm.
    """
    k = len(fp)
    if len(fm) != k:
        raise ValueError('The two lists must contain the same number of '
                         'polynomials.')
    if debug: print(f'k = {k}')
    f = [fp[i] + fm[i] for i in range(k)]
    Mp, Mm = macaulay_sparse_2(fp, fm, cols, debug)
    return lambda y: Mm.sharp(Mp.dot(y)).view(np.ndarray)

#==============================================================================
#------------------------------ Value iteration -------------------------------
#==============================================================================

def b(y, Ty):
    """Given two arrays y and Ty of the same length, computes the infimum of
    Ty[i] - y[i].
    """
    l = len(y)
    bot = inf
    if len(Ty) != l:
        raise ValueError('The two arrays must have the same length.')
    for i in range(l):
        bot = min(bot, Ty[i]-y[i])
    return bot

def t(y, Ty):
    """Given two arrays y and Ty of the same length, computes the supremum of
    Ty[i] - y[i] such that y[i] < +infinity
    """
    l = len(y)
    top = -inf
    if len(Ty) != l:
        raise ValueError('The two arrays must have the same length.')
    for i in range(l):
        if y[i] != inf: top = max(top, Ty[i]-y[i])
    return top

def value_iteration(T: Callable[[np.ndarray], np.ndarray],
                    y0: np.ndarray,
                    iter_max: int,
                    error: float,
                    timer: bool = False,
                    display: bool = False,
                    debug: bool = False,
                    ) -> Tuple[np.ndarray, np.ndarray, int,
                               Optional[bool], bool]:
    """Execute the value iteration with widening algorithm.

    Call:
        >>> y, Ty, nb_iter, success, widening = value_iteration(
                                               T, y0, iter_max, error)
    Input:
        - T (function): the Shapley operator which is used to perform the value
        iteration algorithm.
        - y0 (1D ndarray): the starting point of the value iteration algorithm.
        - iter_max (int): the maximal number of iterations performed by the
        value iteration algorithm.
        - error (float): the approximation error allowed when evaluating
        inequalities in the value iteration algorithm.
        - timer (bool, default False): for activating a timer.
        - display (bool, default False): for activating the display mode, which
        prints out nicely the result of the algorithm.
        - debug (bool, default False): for activating the debug mode, which
        displays the contents of different intermediate variables.

    Output:
        - y (1D ndarray): the vector obtained after the last but one iteration.
        - Ty (1D ndarray): the vector obtained after the last iteration.
        - nb_iter (int): the total number of iterations performed by the
        algorithm.
        - success (bool or None): the result of the value iteration algorithm:
        if the algorithm succesfully converges then success = True, if the value
        iteration does not converge then success = False, and if the maximal
        number of iterations is attained without reaching a conclusion, then
        success = None.
        - widening (bool): whenever the algorithm converges, widening = True
        if the conclusion was reached using the widening, otherwise widening =
        False (in particular widening = False whenever the algorithm does not
        converge or fails to decide).
    """
    tInit = time()
    y = y0
    for i in range(1, iter_max+1):
        Ty = np.minimum(y, (y+T(y))/2)
        if debug: print(f'y = {y}\nTy ={Ty}')
        # bot = min(Ty - y)
        bot = b(y, Ty)
        if bot >= -error:
        # if (Ty == y).all():
            tEnd = time()
            if debug: print('')
            if display: print(f'Algorithm executed in {i} iterations.\n'
                              f'A finite solution was found: y = {Ty}.')
            if timer: print(f'Execution time: {tEnd-tInit:.4e}s.\n')
            return y, Ty, True, i, False
        # top = max(Ty - y)
        top = t(y, Ty)
        if top < -error:
        # if top < 0:
            tEnd = time()
            if debug: print('')
            if display: print(f'Algorithm executed in {i} iterations.\n'
                              f'No solution was found, the algorithm loops '
                              f'with vector y = {Ty}.')
            if timer: print(f'Execution time: {tEnd-tInit:.4e}s.\n')
            return y, Ty, False, i, False
        z = np.where(Ty - y >= -error, inf, y)
        # z = np.where(Ty != y, inf, y)
        if debug: print(f'z = {z}')
        Tz = np.array(T(z))
        # Tz = np.minimum(z, T(z))
        if debug: print(f'Tz ={Tz}')
        # finite_indices = (z != inf)
        # wtop = max(Tz[finite_indices] - z[finite_indices])
        wtop = t(z, Tz)
        if wtop < -error:
        # if wtop < 0:
            tEnd = time()
            if debug: print('')
            if display: print(f'Algorithm executed in {i} iterations.\n'
                              f'No solution was found, the algorithm loops '
                              f'after widening with vector y = {Ty}.')
            if timer: print(f'Execution time: {tEnd-tInit:.4e}s.\n')
            return y, Ty, False, i, True
        y = Ty
        if debug: print('')
    tEnd = time()
    if debug: print('')
    if display: print('The maximal number or iterations was reached before '
                      'the algorithm could decide if there is a solution.')
    if timer: print(f'Value iteration executed in: {tEnd-tInit:.4e}s.\n')
    # if debug: print(f'wtop = {wtop}\nbot = {bot}\n')
    return y, Ty, None, i, False

def dichotomy_solve_2(fp: List[tpol.tpoly],
                      fm: List[tpol.tpoly],
                      cols: List[Tuple[int, ...]],
                      iter_max: int,
                      lower_bound: float,
                      upper_bound: float,
                      precision: float,
                      error: float,
                      timer: bool = False,
                      display: bool = False,
                      debug: bool = False,
                      ) -> Tuple[float, ...]:
    """Compute a solution of the inequality fp >= fm using a dichotomy method.

    This function returns an approcimation solution of the system of
    inequalities fp[i](x) >= fm[i](x). This solution is computed using a
    dichotomy method based on the value iteration algorithm.

    Call:
        >>> sol = dichotomy_solve(fp, fm, cols, iter_max,
                                  lower_bound, upper_bound,
                                  precision, error)
    
    Input:
        - fp (list): a list of tropical polynomials representing the
         lefthandside of the inequality fp >= fm.
        - fm (list): a list of tropical polynomials representing the
        righthandside of the inequality fp >= fm.
        - cols (list): a liste of tuples corresponding to the columns of the
        constructed matrix.
        - iter_max (int): the maximal number of iterations performed by the
        value iteration algorithm.
        - lower_bound (float): the lower bound of the window in which the
        algorithm searches for a solution.
        - upper_bound (float): the upper bound of the window in which the
        algorithm searches for a solution.
        - precision (float): the precision of the approximation of the solution
        computed by the algorithm.
        - error (float): the approximation error allowed when evaluating
        inequalities in the value iteration algorithm.
        - timer (bool, default False): for activating a timer.
        - display (bool, default False): for activating the display mode, which
        prints out nicely the result of the algorithm.
        - debug (bool, default False): for activating the debug mode, which
        displays the contents of different intermediate variables.

    Output:
        - sol (tuple): an approximate solution to the system fp >= fm.
    """
    k = len(fp)
    if len(fm) != k:
        raise ValueError('The two lists must contain the same number of '
                         'polynomials.')
    if debug: print(f'k = {k}\n')
    f = [fp[i] + fm[i] for i in range(k)]
    n = f[0].nb_variables
    list_var = f[0].variables
    for i in range(1, k):
        if f[i].variables != list_var:
            raise ValueError('All polynomials must have the same variables')
    # Maybe raise an error if the elements of cols are not n-tuples.
    var = ' '.join(list_var)
    if debug: print(f'Generation of the Macaulay matrix')
    t1 = time()
    Mp_split, Mm_split = macaulay_sparse_2_dichotomy(fp, fm, cols)
    t2 = time()
    if timer: print(f'Generation of the Macaulay matrices in {t2-t1:.4e}s.\n')
    Mp_incr = Mp_split[0]
    Mm_incr = Mm_split[0]
    T_incr = shapley_2(Mp_incr, Mm_incr)
    zero = np.zeros(len(cols))
    y0 = zero
    if debug: print('Research of the existence of a solution.')
    t3 = time()
    _, _, solution_exists, nb_iter, _ = value_iteration(
                                        T_incr, y0, iter_max, error)
    # TODO : check whether there is a solution with all coordinates within the
    # provided bounds instead
    t4 = time()
    if debug: print(f'Existence of a solution found in {nb_iter} iterations.')
    if timer: print(f'Determination of the existence of a solution in '
                    f'{t4-t3:.4e}s.\n')
    if not solution_exists:
        raise ValueError('The equation does not have a solution.')
    # Fp = fp
    # Fm = fm
    sol = ()
    # g = []
    # for i in range(n):
    #     e_i = [0 for i in range(n)]
    #     e_i[i] = 1
    #     g += [tpol.tpoly({tuple(e_i): 0}, var)]
    # if debug: print(f'g = {g}')
    for i in range(n):
        print(f'Computing coordinate number {i}.\n')
        y0 = zero
        t5 = time()
        if debug: print('Research of the existence of a solution within the '
                        'provided bounds.')
        a = lower_bound
        # ga = tpol.tpoly({tuple(0 for i in range(n)): a}, var)
        b = upper_bound
        # gb = tpol.tpoly({tuple(0 for i in range(n)): b}, var)
        Mp = Mp_incr + (b*Mp_split[2*i+1]) + Mp_split[2*i+2]
        Mm = Mm_incr + Mm_split[2*i+1] + (a*Mm_split[2*i+2])
        T = shapley_2(Mp,Mm)
        # TODO: le fait de recalculer T avec toute la grosse matrice à
        # répétition est coûteux, mieux vaut procéder comme dans le cas
        # balance en faisant le minimum de deux opérateurs.
        _, y0, solution_in_interval, nb_iter, _ = value_iteration(
                                                  T, y0, iter_max, error)
        if not solution_in_interval:
            raise ValueError('There is no solution within the provided '
                             'bounds.')
        if debug: print(f'Solution found within the provided bounds in '
                        f'{nb_iter} iterations.\n')
        while b - a > precision:
            c = (a+b)/2
            # gc = tpol.tpoly({tuple(0 for i in range(n)): c}, var)
            if debug: print(f'a = {a}\nb = {b}\nc = {c}')
            Mp = Mp_incr + (b*Mp_split[2*i+1]) + Mp_split[2*i+2]
            Mm = Mm_incr + Mm_split[2*i+1] + (c*Mm_split[2*i+2])
            T = shapley_2(Mp, Mm)
            _, y0, solution_found, nb_iter, widening = value_iteration(
                                                       T, y0, iter_max, error)
            # TODO : choisir un bon y0 avec un démarrage à chaud
            if solution_found is None:
                raise ValueError('Maximal number of iterations attained.')
            # _, _, _, nb_iter_cold, widening_cold = value_iteration(
            #                                        T, zero, iter_max, error)
            if solution_found is True:
                a = c
                # ga = gc
                if debug:
                    print(f'Solution found in {nb_iter} iterations '
                          f'with warm start.')
                    # print(f'Solution found in {nb_iter_cold} iterations '
                    #       f'with cold start.')
                    print(f'Solution found with x[{i}] in [c,b]\n')
            else:
                b = c
                # gb = gc
                if debug:
                    print(f'Solution found in {nb_iter} iterations '
                          f'with warm start.')
                    if widening: print('Concluded with widening.')
                    # print(f'Solution found in {nb_iter_cold} iterations '
                    #       f'with cold start.')
                    # if widening_cold: print('Concluded with widening.')
                    print(f'Solution found with x[{i}] in [a,c]\n')
        # Fp += [gb, g[i]]
        # Fm += [g[i], ga]
        Mp_incr = Mp_incr + (b*Mp_split[2*i+1]) + Mp_split[2*i+2]
        # TODO : use __iadd__
        Mm_incr = Mm_incr + Mm_split[2*i+1] + (a*Mm_split[2*i+2])
        # TODO : use __iadd__
        c = (a+b)/2
        print(f'x[{i}] = {c}.')
        sol += (c,)
        t6 = time()
        if timer:
            print(f'Computation of coordinate number {i} in {t6-t5:.4e}s.\n')
    if display:
        print(f'Solution found: x = {sol}\n')
        for i in range(k):
            print(f'fp[{i}](x) = {fp[i](sol)}\nfm[{i}](x) = {fm[i](sol)}\n')
    tEnd = time()
    return sol

def VIWW(fp,fm,cols,nb_iter,epsilon,timer=True,display=True,debug=False):
    T = shapley_sparse_2(fp,fm,cols,debug)
    y = np.zeros(len(cols))
    t1 = time()
    for i in range(nb_iter):
        Ty = np.minimum(y,(y+T(y))/2)
        if debug: print(f'Ty =\n{Ty}\n\n')
        # bot = min(Ty - y)
        bot = b(y,Ty)
        if bot >= -epsilon :
        # if (Ty == y).all():
            t2 = time()
            if timer: print(f't2 - t1 = {t2-t1}\n')
            if display: print(f'Algorithm executed in {i+1} iterations.\nThe system fp \u2265 fm has a finite root.')
            return y,Ty,True,i+1
        # top = max(Ty - y)
        top = t(y,Ty)
        if top < -epsilon :
        # if top < 0:
            t2 = time()
            if timer: print(f't2 - t1 = {t2-t1}\n')
            if display: print(f'Algorithm executed in {i+1} iterations.\nThe system fp \u2265 fm does not have a finite root.')
            return y,Ty,False,i+1
        z = np.where(Ty - y >= -epsilon, inf, y)
        # z = np.where(Ty != y, inf, y)
        if debug: print(f'z = {z}')
        Tz = np.array(T(z))
        # Tz = np.minimum(z,T(z))
        if debug: print(f'Tz = {Tz}')
        # finite_indices = (z != inf)
        # wtop = max(Tz[finite_indices] - z[finite_indices])
        wtop = t(z,Tz)
        if wtop < -epsilon:
        # if wtop < 0:
            t2 = time()
            if timer: print(f't2 - t1 = {t2-t1}\n')
            if display: print(f'Algorithm executed in {i+1} iterations.\nThe system fp \u2265 fm does not have a finite root (widening).')
            return y,Ty,False,i+1
        y = Ty
    t2 = time()
    if timer: print(f't2 - t1 = {t2-t1}\n')
    if display: print(f'Algorithm executed in {i+1} iterations.\nThe algorithm did not determine if the system fp \u2265 fm has a finite root.')
    # if debug: print(f'wtop = {wtop}\nbot = {bot}\n')
    return y,Ty,None,nb_iter

def dichotomy_brute(fp,fm,cols,nb_iter,lower_bound,upper_bound,
                    precision,epsilon,debug=False):
    k = len(fp)
    if len(fm) != k: raise ValueError('The two lists must contain the same number of polynomials.')
    if debug: print(f'k = {k}')
    f = [fp[i] + fm[i] for i in range(k)]
    n = f[0].nb_variables
    list_var = f[0].variables
    _,_,solution_exists,_ = VIWW(fp,fm,cols,nb_iter,epsilon,timer=False,display=False)
    if solution_exists is False: raise ValueError('The equation does not have a solution.')
    for i in range(1,k):
        if f[i].variables != list_var: raise ValueError('All polynomials must have the same variables')
    # Maybe raise an error if the elements of cols are not n-tuples.
    var = ' '.join(list_var)
    Fp = deepcopy(fp)
    Fm = deepcopy(fm)
    sol = ()
    g = []
    for i in range(n):
        e_i = [0 for i in range(n)]
        e_i[i] = 1
        g += [tpol.tpoly({tuple(e_i): 0},var)]
    if debug: print(f'g = {g}')
    for i in range(n):
        a = lower_bound
        ga = tpol.tpoly({tuple(0 for i in range(n)): a},var)
        b = upper_bound
        gb = tpol.tpoly({tuple(0 for i in range(n)): b},var)
        _,_,solution_in_interval,_ = VIWW(Fp + [gb,g[i]],Fm + [g[i],ga],cols,nb_iter,epsilon,timer=False,display=False)
        if solution_in_interval is False: raise ValueError('There is no solution within the provided bound.')
        while b - a > precision:
            c = (a+b)/2
            gc = tpol.tpoly({tuple(0 for i in range(n)): c},var)
            if debug: print(f'a = {a}\nb = {b}\nc = {c}')
            _,_,solution_found,_ = VIWW(Fp + [gb,g[i]],Fm + [g[i],gc],cols,nb_iter,epsilon,timer=False,display=False) # Très très inefficace !!!
            # Il ne faut pas recalculer toute la matrice de Macaulay à chaque étape de la dichotomie sous peine d'une explosion de la complexité.
            if solution_found is None: raise ValueError('Maximal number of iterations attained.')
            elif solution_found:
                a = c
                ga = gc
                if debug: print('Solution found in [c,b]\n\n')
            else:
                b = c
                gb = gc
                if debug: print('Solution found in [a,c]\n\n')
        Fp += [gb,g[i]]
        Fm += [g[i],ga]
        c = (a+b)/2
        sol += (c,)
    return sol

#==============================================================================
#----------------------------------- Tests ------------------------------------
#==============================================================================

def test():
    # fp1 = tpol.tpoly({(0, 0): 1, (0, 1): 1},'x y')
    # fp2 = tpol.tpoly({(0, 1): 1},'x y')
    # fp3 = tpol.tpoly({(1, 0): 2},'x y')
    # fp = [fp1, fp2, fp3]
    # print(f'fp = {fp}')

    # fm1 = tpol.tpoly({(1, 0): 4, (1, 1): 3},'x y')
    # fm2 = tpol.tpoly({(0, 0): 0, (1, 0): 0},'x y')
    # fm3 = tpol.tpoly({(0, 1): 0},'x y')
    # fm = [fm1, fm2, fm3]
    # print(f'fm = {fm}')

    # cols = iter_mons(2, 2)
    # Mp, Mm = macaulay_sparse_2(fp, fm, cols)
    # T = shapley(Mp, Mm)
    # y0 = np.zeros(len(cols))
    # y, Ty, succes, nb_iter, widening = value_iteration(T, y0, 500, 0.001,
    #                                                    timer=True)
    # print(f'y = {y}\nTy ={Ty}\nsucces = {succes}\nnb_iter = {nb_iter}\n')
    # Mp_split, Mm_split = macaulay_sparse_2_dichotomy(fp, fm, cols)
    # # print(f'Mp_split =\n{Mp_split}\n\nMm_split =\n{Mm_split}\n')
    # dichotomy_solve(fp, fm, iter_mons(2, 2), 1000, -100, 100,
    #                 10**(-3), 10**(-6), display=True)

    # TODO : Mettre les tests dans un fichier à part

    k = 3
    var = 'x y'
    n = len(var.split())
    deg_max = 10
    spar_max = 4
    fp, fm = [], []
    list_deg = []
    for i in range(k):
        deg = randint(0, deg_max)
        list_deg += [deg]
        spar = randint(1, min(spar_max, binom(deg+n-1, n-1)))
        fp += [tpol.rand_tpoly(var, deg, spar, bound=1)]
        print(f'fp[{i}] = {fp[i]}')
    print('')
    for i in range(k):
        deg = randint(0, deg_max)
        list_deg[i] = max(deg, list_deg[i])
        spar = randint(1, min(spar_max, binom(deg+n-1, n-1)))
        fm += [tpol.rand_tpoly(var, deg, spar, bound=1)]
        print(f'fm[{i}] = {fm[i]}')
    print('')
    N = 3*max(list_deg)
    
    print(f'N = {N}\n')
    dichotomy_solve_2(fp, fm, iter_mons(n, N), 10000, -1000, 1000,
                      10**(-4), 10**(-6), timer=True, display=True, debug=True)

    