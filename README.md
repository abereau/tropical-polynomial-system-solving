# Tropical Polynomial System Solving

## Description

The **Tropical Polynomial System Solving** project consists in a Python implementation of tropical polynomial and tropical matrices — either full or sparse — as classes, and provides some base tools to work with these objects. The end goal of this project is to implement sparse a tropical polynomial system solver.

As for now, this repository contains a folder `tropical` which contains the different Python modules for performing tropical computations, as well as a file `feasible-instances.py`, which tests the solvability of a given number of instances of randomly generated sparse tropical polynomial systems when run.

## Installation

To be able to run this code, you need to install first

https://github.com/tulip-control/polytope/tree/main/polytope

which you can do with pip by running

```
pip install git+https://github.com/tulip-control/polytope
```

## Authors and acknowledgment

&copy; [Antoine Béreau](https://www.antoine-bereau.fr) ([antoine.bereau@inria.fr](mailto:antoine.bereau@inria.fr)), École polytechnique (2023)

This project is being realized in the context of my ongoing thesis research at École polytechnique under the supervision of [Marianne Akian](http://www.cmap.polytechnique.fr/~akian/) and [Stéphane Gaubert](http://www.cmap.polytechnique.fr/~gaubert/), which is the foundation of the theoretical content on which relies this implementation. You can find the related paper on HAL [here](https://inria.hal.science/hal-04117544).

I also need to thank my friend Baptiste Desoubrie, whose advice and technical knowledge in programming were of valuable help in making this Python code as polished as possible.

## License

GNU LESSER GENERAL PUBLIC LICENSE (see the file `LICENSE.txt`)