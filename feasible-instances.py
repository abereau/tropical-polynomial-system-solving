# This Python script computes the ratio of feasible instances for random
# systems of k tropical polynomial equations in n variables among a given
# number of samples, for different values of k and n.

from filelock import FileLock
from itertools import product, repeat
import numpy as np
from multiprocessing import Pool
import os
from pandas import DataFrame
from polytope import solvers
import polytope.polytope as alg
from random import randint
from time import time
from tqdm import tqdm
from typing import List, Literal, Optional, Tuple

from tropical.polynomials import rand_tpoly, tpoly
from tropical.macaulay import iter_mons, macaulay_sparse_2, tuple_add
from tropical.games import shapley_2, value_iteration
# import tropical.porta as porta

inf = float('inf')
solvers.default_solver = 'scipy'

#==============================================================================
#------------------------------- Test function --------------------------------
#==============================================================================

def test_poly_full(var: str,
                   nb_eq: int,
                   deg: int,
                   spar: int,
                   iter_max: int,
                   error: float,
#                   dil: int = 1,
                   mode: Literal[0, 1] = 0,
                   timer: bool = False,
                   display: bool = True,
                   debug: bool = False
                   ) -> Tuple[np.ndarray, np.ndarray, int,
                              Optional[bool], bool]:
    """Generate a random polynomial system and tests whether it is solvable.
     
    This function generates two lists fp and fm of random tropical polynomials
    and returns the result of the value iteration algorithm (with widening) on
    the system of tropical polynomial inequalities fp[i] >= fm[i] for all i.

    Call:
        >>> y, Ty, nb_iter, succes, widening = test_poly(var, deg, spar,
                                                         nb_iter, error)
    
    Input:
        - var (str): the variables of the generated polynomials.
        - nb_eq (int): the number of inequations in the system.
        - deg (int): the degree of the generated polynomials.
        - spar (int): the sparsity of the generated polynomials.
        - iter_max (int): the maximal number of iterations performed by the
        value iteration algorithm.
        - error (float): the approximation error allowed when evaluating
        inequalities in the value iteration algorithm.
#        - dil (int): the dilation applied to the Minkowski sum of the supports
        of the polynomials of the system before the selection of the columns of
        the Macaulay matrices.
        - mode (int, default 0): 0 or 1, mode=0 is for generating tropical
        polynomials with integer coefficients, mode=1 is for generating
        tropical polynomials with real coefficients.
        - display (bool, default True): for activating the display mode, which
        prints out nicely the result of the algorithm.
        - debug (bool, default False): for activating the debug mode, which
        displays the contents of different intermediate variables.
    
    Output:
        - y (1D ndarray): the vector obtained after the last but one iteration.
        - Ty (1D ndarray): the vector obtained after the last iteration.
        - nb_iter (int): the total number of iterations performed by the
        algorithm.
        - succes (bool or None): the result of the value iteration algorithm:
        if the algorithm succesfully converges then succes = True, if the value
        iteration does not converge then succes = False, and if the maximal
        number of iterations is attained without reaching a conclusion, then
        succes = None.
        - widening (bool): whenever the algorithm converges, widening = True
        if the conclusion was reached using the widening, otherwise widening =
        False (in particular widening = False whenever the algorithm does not
        converge or fails to decide).

    """
    n = len(var.split())
    k = nb_eq
    fp, fm = [], []
    for i in range(k):
        fp += [rand_tpoly(var, deg, spar, mode)]
        if display: print(f'fp[{i}] = {fp[i]}')
    if display: print('')
    for i in range(k):
        fm += [rand_tpoly(var, deg, spar, mode)]
        if display: print(f'fm[{i}] = {fm[i]}')
    if display: print('')
    list_deg = [max(fp[i].degree(),fm[i].degree()) for i in range(k)]
    if debug: print(f'list_deg = {list_deg}')
#    N = dil*sum(list_deg)
    N = sum(list_deg)
    if debug: print(f'N = {N}')
    cols = iter_mons(n,N)
    nb_rows = sum(len(iter_mons(n,N-list_deg[i],i)) for i in range(k)) 
    nb_cols = len(cols)
    if display:
        print(f'The full Macaulay matrix has size '
              f'{nb_rows} \u00d7 {nb_cols}\n')
    Mp, Mm = macaulay_sparse_2(fp, fm, cols, timer, debug)
    T = shapley_2(Mp, Mm)
    y0 = np.zeros(nb_cols)
    return (*value_iteration(T, y0, iter_max, error, timer, display, debug),
            nb_rows,
            nb_cols)

def test_poly_sparse(var: str,
                     nb_eq: int,
                     deg: int,
                     spar: int,
                     iter_max: int,
                     error: float,
#                     dil: int = 1,
                     mode: Literal[0, 1] = 0,
                     timer: bool = False,
                     display: bool = True,
                     debug: bool = False
                     ) -> Tuple[np.ndarray, np.ndarray, int,
                              Optional[bool], bool]:
    """Generate a random polynomial system and tests whether it is solvable.
     
    This function generates two lists fp and fm of random tropical polynomials
    and returns the result of the value iteration algorithm (with widening) on
    the system of tropical polynomial inequalities fp[i] >= fm[i] for all i.

    Call:
        >>> y, Ty, nb_iter, succes, widening = test_poly(var, deg, spar,
                                                         nb_iter, error)
    
    Input:
        - var (str): the variables of the generated polynomials.
        - k (int): the number of inequations in the system.
        - deg (int): the degree of the generated polynomials.
        - spar (int): the sparsity of the generated polynomials.
        - iter_max (int): the maximal number of iterations performed by the
        value iteration algorithm.
        - error (float): the approximation error allowed when evaluating
        inequalities in the value iteration algorithm.
#        - dil (int): the dilation applied to the Minkowski sum of the supports
        of the polynomials of the system before the selection of the columns of
        the Macaulay matrices.
        - mode (int, default 0): 0 or 1, mode=0 is for generating tropical
        polynomials with integer coefficients, mode=1 is for generating
        tropical polynomials with real coefficients.
        - display (bool, default True): for activating the display mode, which
        prints out nicely the result of the algorithm.
        - debug (bool, default False): for activating the debug mode, which
        displays the contents of different intermediate variables.
    
    Output:
        - y (1D ndarray): the vector obtained after the last but one iteration.
        - Ty (1D ndarray): the vector obtained after the last iteration.
        - nb_iter (int): the total number of iterations performed by the
        algorithm.
        - succes (bool or None): the result of the value iteration algorithm:
        if the algorithm succesfully converges then succes = True, if the value
        iteration does not converge then succes = False, and if the maximal
        number of iterations is attained without reaching a conclusion, then
        succes = None.
        - widening (bool): whenever the algorithm converges, widening = True
        if the conclusion was reached using the widening, otherwise widening =
        False (in particular widening = False whenever the algorithm does not
        converge or fails to decide).

    """
    k = nb_eq
    fp, fm = [], []
    for i in range(k):
        fp += [rand_tpoly(var, deg, spar, mode)]
        if display: print(f'fp[{i}] = {fp[i]}')
    if display: print('')
    for i in range(k):
        fm += [rand_tpoly(var, deg, spar, mode)]
        if display: print(f'fm[{i}] = {fm[i]}')
    if display: print('')
    # f = [fp[i] + fm[i] for i in range(k)]
    # list_supports = [f[i].support for i in range(k)]
    # if debug: print(f'list_supports = {list_supports}')
    # porta.minkowski_sum_as_ieq_file(list_supports, 'minkowski.ieq')
    # os.system('valid -V minkowski.ieq')
    # poi_file = open('minkowski.poi', 'r')
    # cols = porta.get_list_points_from_poi(poi_file)
    # poi_file.close()
    # os.remove('minkowski.ieq')
    # os.remove('minkowski.poi')
    cols = integer_points(newton_polytope_system_incr(fp, fm))
    nb_cols = len(cols)
    if display:
        print(f'The full Macaulay matrix has {nb_cols} columns.\n')
    Mp, Mm = macaulay_sparse_2(fp, fm, cols, timer, debug)
    T = shapley_2(Mp, Mm)
    y0 = np.zeros(nb_cols)
    return (*value_iteration(T, y0, iter_max, error, timer, display, debug),
            *Mp.shape)

def newton_polytope_system_incr(fp: List[tpoly],
                                fm: List[tpoly],
                                debug: bool = False
                                ) -> alg.Polytope:
    """Return the Minkowski sum of the Newton polytopes of a polynomial system.
    
    This functions takes a pair of list fp and fm of tropical polynomials, and
    returns the Minkowski sum of the Newton polytopes of the polynomials fp[i]
    ⨁ fm[i], computed incrementally.

    Call:
        >>> Q = newton_polytope_system_incr(fp, fm)
    
    Input:
        - fp (list): a list of tropical polynomials.
        - fm (list): a list of tropical polynomials.
    
    Output:
        - Q (Polytope): the Minkowski sum of the Newton polytopes of the
        polynomials fp[i] ⨁ fm[i].
    """
    k = len(fp)
    if len(fm) != k:
        raise ValueError('The two lists must contain the same number of '
                         'polynomials.')
    if debug: print(f'k = {k}')
    list_supports = [
        list(set(fp[i].support).union(set(fm[i].support))) for i in range(k)
        ]
    if debug: print(f'list_supports = {list_supports}')
    list_vertices = list_supports[0]
    for i in range(1, k):
        list_vertices = list(set([
            tuple_add(a, b) for (a, b) in product(list_vertices,
                                                  list_supports[i])]))
    return alg.qhull(np.array(list_vertices))

def feasible_instances(nb_tests: int,
                       var: str,
                       nb_eq: int,
                       deg: int,
                       spar: int,
                       iter_max: int,
                       error: float,
                       filename: str,
                       sparse: bool = True,
#                       dil: int = 1,
                       mode: Literal[0, 1] = 0,
                       timer: bool = False,
                       display: bool = False,
                       debug: bool = False
                       ) -> None:
    """Solve a number of tropical polynomial systems and print the results.

    This procedure solves a given number of randomly generated instances of
    systems of tropical polynomial inequalities, and prints out in a file for
    the output of each instance, the number of iterations performed by the
    value iteration algorithm as well as the time takes, and then prints out
    several stats, among which the number of feasible instances and the overall
    average number of iteration and time taken to solve an instance.

    Call:
        >>> feasible_instances(nb_tests, var, nb_eq, deg, spar, iter_max,
                               error, filename)
    
    Input:
        -nb_tests (int)
        - var (str): the variables of the generated polynomials.
        - nb_eq (int): the number of inequations in the system.
        - deg (int): the degree of the generated polynomials.
        - spar (int): the sparsity of the generated polynomials.
        - iter_max (int): the maximal number of iterations performed by the
        value iteration algorithm.
        - error (float): the approximation error allowed when evaluating
        inequalities in the value iteration algorithm.
        - filename (str)
        - sparse (bool)
#        - dil (int): the dilation applied to the Minkowski sum of the supports
        of the polynomials of the system before the selection of the columns of
        the Macaulay matrices.
        - mode (int, default 0): 0 or 1, mode=0 is for generating tropical
        polynomials with integer coefficients, mode=1 is for generating
        tropical polynomials with real coefficients.
        - display (bool, default True): for activating the display mode, which
        prints out nicely the result of the algorithm.
        - debug (bool, default False): for activating the debug mode, which
        displays the contents of different intermediate variables.
    """
    n = len(var.split())
    k = nb_eq
    if os.path.exists(filename):
        file = open(filename, 'a')
        file.write('\n'
                   '======================================================\n'
                   '\n')
    else:
        file = open(filename, 'x')
    file.write(f'Parameters:\n'
               f'\n'
               f'  - number of tests: {nb_tests:>33}\n'
               f'  - number of variables: {n:>29}\n'
               f'  - number of equations: {k:>29}\n'
               f'  - degree: {deg:>42}\n'
               f'  - sparsity: {spar:>40}\n'
               f'  - maximal number of iterations: {iter_max:>20}\n'
               f'  - error for the comparation tests: {error:>17.1e}\n'
               f'  - operator used: {"sparse" if sparse else "full":>35}\n'
               f'\n'
               f'Output:\n'
               f'\n'
               f'  no. test  |  result  |  iterations  |  time in sec  \n'
               f'------------|----------|--------------|---------------\n')
    min_iter = iter_max
    max_iter = 0
    sum_iter = 0
    min_iter_false = iter_max
    max_iter_false = 0
    sum_iter_false = 0
    min_iter_true = iter_max
    max_iter_true = 0
    sum_iter_true = 0
    total_time = 0
    tab_res = np.zeros((1, 3)).astype(int)
    for i in tqdm(range(nb_tests)):
        if sparse:
            start = time()
            _, _, res, nb_iter, _, _, _ = test_poly_sparse(
                var, k, deg, spar, iter_max, error,
                mode, timer, display, debug
                )
            end = time()
        else:
            start = time()
            _, _, res, nb_iter, _, _, _ = test_poly_full(
                var, k, deg, spar, iter_max, error,
                mode, timer, display, debug
                )
            end = time()
        file.write(f'  {i+1:>8}  |  {str(res):>6}  |  {nb_iter:>10}  |'
                   f'  {end - start:>11.7f}  \n')
        # if display:
        #     print(f'Result of test number {i+1}: {res}.\n'
        #           f'Number of iterations required: {nb_iter}.\n')
        
        min_iter = min(min_iter, nb_iter)
        max_iter = max(max_iter, nb_iter)
        sum_iter += nb_iter
        total_time += end - start
        if res is False:
            min_iter_false = min(min_iter_false, nb_iter)
            max_iter_false = max(max_iter_false, nb_iter)
            sum_iter_false += nb_iter
            tab_res[0, 0] += 1
        elif res is None:
            tab_res[0, 1] += 1
        elif res is True:
            min_iter_true = min(min_iter_true, nb_iter)
            max_iter_true = max(max_iter_true, nb_iter)
            sum_iter_true += nb_iter
            tab_res[0, 2] += 1
    col_labels = ['False', 'None', 'True']
    res_frame = DataFrame(tab_res, columns=col_labels, index=[''])
    file.write(f'\n'
               f'Overall results:\n'
               f'\n'
               f'{res_frame}\n'
               f'\n'
               f'Minimal number of iterations: {min_iter:>24}\n'
               f'Maximal number of iterations: {max_iter:>24}\n'
               f'Average number of iterations: {sum_iter/nb_tests:>24.0f}\n'
               f'\n'
               f'Minimal number of iterations when infeasible: '
               f'{min_iter_false:>8}\n'
               f'Maximal number of iterations when infeasible: '
               f'{max_iter_false:>8}\n'
               f'Average number of iterations when infeasible: '
               f'{sum_iter_false/tab_res[0, 0]:>8.0f}\n'
               f'\n'
               f'Minimal number of iterations when feasible: '
               f'{min_iter_true:>10}\n'
               f'Maximal number of iterations when feasible: '
               f'{max_iter_true:>10}\n'
               f'Average number of iterations when feasible: '
               f'{sum_iter_true/tab_res[0, 2]:>10.0f}\n'
               f'\n'
               f'Average time per instance: {total_time/nb_tests:>26.7f}s\n')
    file.close()

def integer_points(Q):
    """ Renvoie la liste des points entiers, représentés par des tuples, d'un polytope Q.
    """
    return list(map(tuple, np.transpose(alg.enumerate_integral_points(Q).astype(int))))

#==============================================================================
#--------------------------------- Benchmarks ---------------------------------
#==============================================================================

def timed_test(var: str,
               nb_eq: int,
               deg: int,
               spar: int,
               iter_max: int,
               error: float,
               data_file_name: str,
               ) -> Tuple[Optional[bool], int, float]:
    """
    """
    start = time()
    _, _, res, nb_iter, _, nb_rows, nb_cols = test_poly_sparse(
        var, nb_eq, deg, spar, iter_max, error,
        mode=0, timer=False, display=False, debug=False
        )
    end = time()

    # Write results in file raw_data.txt
    lock = FileLock(data_file_name + '.lock')
    with lock:
        with open(data_file_name, 'r') as file:
            last_line = file.readlines()[-1].split('|')
        try:
            index = int(last_line[0]) + 1
        except ValueError:
            index = 1
        with open(data_file_name, 'a') as file:
            file.write(
                f'  {index:>8}  |  {nb_rows:>8}  |  {nb_cols:>8}  |'
                f'  {str(res):>6}  |  {nb_iter:>8}  |{end-start:>12.3f}  \n'
                )
    return nb_rows, nb_cols, res, nb_iter, end - start

def benchmarks(nb_tests: int,
               nb_var_min: int,
               nb_var_max: int,
               nb_eq_min: int,
               nb_eq_max: int,
               deg: int,
               spar: int,
               iter_max: int,
               error: float,
               data_file_name: str = 'raw_data.txt',
               stats_file_name: str = 'stats.txt',
               file_mode: str = 'a',
               ) -> None:
    """
    """
    with open(data_file_name, file_mode) as data_file:
        data_file.write(
            f'Fixed parameters:\n'
            f'\n'
            f'  - number of tests: {nb_tests:>33}\n'
            f'  - degree: {deg:>42}\n'
            f'  - sparsity: {spar:>40}\n'
            f'  - maximal number of iterations: {iter_max:>20}\n'
            f'  - error for the comparation tests: {error:>17.1e}\n'
            f'  - operator used: {"sparse":>35}\n'
            )
    with open(stats_file_name, file_mode) as stat_file:
        stat_file.write(
            f'Fixed parameters:\n'
            f'\n'
            f'  - number of tests: {nb_tests:>33}\n'
            f'  - degree: {deg:>42}\n'
            f'  - sparsity: {spar:>40}\n'
            f'  - maximal number of iterations: {iter_max:>20}\n'
            f'  - error for the comparation tests: {error:>17.1e}\n'
            f'  - operator used: {"sparse":>35}\n'
            f'\n'
            )
    for n, k in sorted(product(range(nb_var_min, nb_var_max+1),
                               range(nb_eq_min, nb_eq_max+1)), key=sum):
        with open(data_file_name, 'a') as data_file:
            data_file.write(
                f'\n'
                f'Output for {n} variables and {k} equations:\n'
                f'\n'
                f'Key:   no. test   =  number of the test\n'
                f'       nb. rows   =  number of rows of the Macaulay matrix\n'
                f'       nb. cols   =  number of colums of the Macaulay matrix\n'
                f'       result     =  outcome of the value iteration algorithm\n'
                f'       nb. iter   =  number of iterations performed by the algorithm.\n'
                f'       time (sec) =  total execution time of the algorithm (for one core)\n'
                f'\n'
                f'  no. test  |  nb. rows  |  nb. cols  |  result  |  nb. iter  |  time (sec)  \n'
                f'------------|------------|------------|----------|------------|--------------\n'
                )
        var = ' '.join([f'x{i}' for i in range(1, n+1)])
        with Pool() as pool:
            results = pool.starmap(timed_test,
                                   repeat((var, k, deg, spar, iter_max, error,
                                           data_file_name),
                                          nb_tests)
                                   )
        # data = DataFrame(results, columns = ['res', 'nb_iter', 'time'])
        tab_rows, tab_cols, tab_res, tab_iter, tab_time = map(np.array, zip(*results))
        mask_false = tab_res == False
        mask_none = tab_res == None
        mask_true = tab_res == True
        nb_false = len(tab_res[mask_false])
        nb_true = len(tab_res[mask_true])
        with open(stats_file_name, 'a') as stat_file:
            stat_file.write(
                f'Output for {n} variables and {k} equations:\n'
                f'\n'
                f'  result                     |    False  |     True  |  overall  \n'
                f'-----------------------------|-----------|-----------|-----------\n'
                f'  occurences                 |  {nb_false:>7}  |  {nb_true:>7}  |  {len(tab_res):>7} \n'
                f'  avg. number of iterations  |  {np.mean(tab_iter[mask_false]) if nb_false else "-":>7{".0f" if nb_false else ""}}  |  {np.mean(tab_iter[mask_true]) if nb_true else "-":>7{".0f" if nb_true else ""}}  |  {np.mean(tab_iter):>7.0f} \n'
                f'  med. number of iterations  |  {np.median(tab_iter[mask_false]) if nb_false else "-":>7{".0f" if nb_false else ""}}  |  {np.median(tab_iter[mask_true]) if nb_true else "-":>7{".0f" if nb_true else ""}}  |  {np.median(tab_iter):>7.0f} \n'
                f'  min. number of iterations  |  {np.min(tab_iter[mask_false]) if nb_false else "-":>7}  |  {np.min(tab_iter[mask_true]) if nb_true else "-":>7}  |  {np.min(tab_iter):>7} \n'
                f'  max. number of iterations  |  {np.max(tab_iter[mask_false]) if nb_false else "-":>7}  |  {np.max(tab_iter[mask_true]) if nb_true else "-":>7}  |  {np.max(tab_iter):>7} \n'
                f'  avg. time for an instance  |{np.mean(tab_time[mask_false]) if nb_false else "-":>9{".3f" if nb_false else ""}}  |{np.mean(tab_time[mask_true]) if nb_true else "-":>9{".3f" if nb_true else ""}}  |{np.mean(tab_time):>9.3f} \n'
                f'\n'
                f'average matrix size: {np.mean(tab_rows)} \u00d7 {np.mean(tab_cols)}\n'
                f'\n'
                f'{len(tab_res[mask_none])} occurence(s) of None\n'
                f'\n'
                )


#==============================================================================
#------------------------------------ Main ------------------------------------
#==============================================================================

if __name__ == '__main__':
    nb_tests    = int(input('Number of tests             :  '))
    nb_var_min  = int(input('Minimal number of variables :  '))
    nb_var_max  = int(input('Maximal number of variables :  '))
    nb_eq_min   = int(input('Minimal number of equations :  '))
    nb_eq_max   = int(input('Maximal number of equations :  '))
    deg         = int(input('Degree                      :  '))
    spar        = int(input('Sparsity                    :  '))
    iter_max    = int(input('Maximal number of iterations:  '))
    error     = float(input('Error                       :  '))
    data_file_name  = input('Data file name              :  ')
    stats_file_name = input('Stats file name             :  ')
    file_mode       = input('File opening mode (use a for appending'
                            ' and w for overwriting):  ')

    data = benchmarks(nb_tests, nb_var_min, nb_var_max, nb_eq_min, nb_eq_max,
                      deg, spar, iter_max, error,
                      data_file_name, stats_file_name, file_mode)